package ru.itis.printer.app;

import ru.itis.printer.utils.Renderer;
import com.beust.jcommander.JCommander;

class Main {
	public static void main(String[] args) {
		Renderer renderer = new Renderer();
		
		Arguments arguments = new Arguments();
		JCommander.newBuilder()
			.addObject(arguments)
			.build()
			.parse(args);

		renderer.print("resources/image.bmp", 
			arguments.black.charAt(0), 
			arguments.white.charAt(0));
	}
}