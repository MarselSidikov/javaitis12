package ru.itis.printer.utils;

import org.junit.Before;
import org.junit.Test;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static org.junit.Assert.*;

public class RendererTest {

    private Renderer renderer;
    private BufferedImage image;

    @Before
    public void setUp() throws Exception {
        renderer = new Renderer();
        InputStream is = new FileInputStream("src/test/resources/image.bmp");
        image = ImageIO.read(is);
    }

    @Test
    public void printForGoodFile() {
        renderer.print(image, '#', '*');
    }

    @Test(expected = NullPointerException.class)
    public void printForBadFile() {
        renderer.print(null, '#', '*');
    }
}