package ru.itis.printer.utils;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Renderer {

    public void print(BufferedImage image, char white, char black) {

        int height = image.getHeight();
        int width = image.getWidth();

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (image.getRGB(j, i) == Color.BLACK.getRGB()) {
                    System.out.print(black);
                } else {
                    System.out.print(white);
                }
            }
            System.out.println();
        }
    }
}
