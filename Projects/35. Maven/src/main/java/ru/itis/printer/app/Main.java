package ru.itis.printer.app;

import com.beust.jcommander.JCommander;
import ru.itis.printer.utils.Renderer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;

class Main {
	public static void main(String[] args) {
		Renderer renderer = new Renderer();
		
		Arguments arguments = new Arguments();
		JCommander.newBuilder()
			.addObject(arguments)
			.build()
			.parse(args);

		String fileName = "/image.bmp";

		BufferedImage image = null;
		try {
			BufferedInputStream file =
					new BufferedInputStream(renderer.getClass().getResourceAsStream(fileName));

			image = ImageIO.read(file);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}

		renderer.print(image,
			arguments.black.charAt(0), 
			arguments.white.charAt(0));
	}
}