public class Ellipse extends Figure {
    private int firstRadius;
    private int secondRadius;

    public Ellipse(int x, int y, int firstRadius, int secondRadius) {
        super(x, y);
        this.firstRadius = firstRadius;
        this.secondRadius = secondRadius;
    }

    public int getFirstRadius() {
        return firstRadius;
    }

    public int getSecondRadius() {
        return secondRadius;
    }

    @Override
    public double getArea() {
        return 2 * Math.PI * firstRadius * secondRadius;
    }
}
