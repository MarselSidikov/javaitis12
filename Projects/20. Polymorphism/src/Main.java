public class Main {

    public static void main(String[] args) {
	    Rectangle rectangle = new Rectangle(1,1 ,10, 5);
	    Square square = new Square(2,2, 7);
	    Ellipse ellipse = new Ellipse(3, 3,2, 3);
	    Circle circle = new Circle(4, 4, 4);

//	    Figure f1 = rectangle;
//	    Figure f2 = square;
//	    Figure f3 = ellipse;
//	    Figure f4 = circle;
//
//	    Figure figures[] = new Figure[4];
//	    figures[0] = f1;
//	    figures[1] = f2;
//	    figures[2] = f3;
//	    figures[3] = f4;

	    Figure figures[] = {rectangle, square, ellipse, circle};

	    for (int i = 0; i < figures.length; i++) {
            System.out.println(figures[i].getArea());
        }

//	    Figure figure = new Figure();
//		System.out.println(figure.getArea());
    }
}
