function getCoursesByDisciplineId(disciplineId) {
    $.ajax({
        method: "get",
        url: "http://localhost:8080/disciplines/" + disciplineId + "/courses",
        headers: {
            "token" : localStorage.getItem("token")
        }
    })
        .done(function(response) {
            fillTable($('#courses_table'), response);
        });
}

function fillTable(table, data) {
    let html = "<table>";
    for (let i = 0; i < data.length; i++) {
        html += "<tr>";
        html += "<td>" + data[i]["title"] + "</td>";
        html += "</tr>";
    }
    html += "</table>";
    table.empty();
    table.append(html);
}