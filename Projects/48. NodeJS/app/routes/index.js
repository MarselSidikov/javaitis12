const usersRoutes = require('./users_routes');
const coursesRoutes = require('./courses_routes');

module.exports = function(app, pool) {
    usersRoutes(app);
    coursesRoutes(app, pool);
};