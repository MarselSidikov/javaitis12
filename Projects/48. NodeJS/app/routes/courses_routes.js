module.exports = function (app, pool) {
    app.get('/courses', (request, response) => {
        let responseBody;

        pool.query('select * from course_table', (error, result) => {
            pool.end();
            responseBody = [{
                "title": result.rows[0].title_of_course,
            }, {
                "title": result.rows[1].title_of_course,
            }];

            response.setHeader("Content-Type", "application/json");
            response.send(JSON.stringify(responseBody));
        });
    });
};