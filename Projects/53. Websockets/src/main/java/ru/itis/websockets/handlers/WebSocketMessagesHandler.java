package ru.itis.websockets.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import ru.itis.websockets.dto.Message;

import java.util.HashMap;
import java.util.Map;

@Component
@EnableWebSocket
public class WebSocketMessagesHandler extends TextWebSocketHandler {

    // список всех клиент-серверных сессий
    private static final Map<String, WebSocketSession> sessions = new HashMap<>();

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        // получает содержимое сообщение
        String messageText = (String) message.getPayload();
        // конвертируем JSON-строку в Java-объект
        Message messageFromJson = objectMapper.readValue(messageText, Message.class);
        // если такой сессии у нас еще не было
        if (!sessions.containsKey(messageFromJson.getFrom())) {
            // кладем его в список сессий
            sessions.put(messageFromJson.getFrom(), session);
        }
        // пробегаемся по всем сессиям и отправляем сообщение клиентам
        for (WebSocketSession currentSession : sessions.values()) {
            currentSession.sendMessage(message);
        }
    }
}
