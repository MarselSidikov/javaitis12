package ru.itis;

public class MergeSort {

    // вспомогательный массив
    private static int helpArray[];

    static {
        helpArray = new int[200];
    }

    // array - исходный массив
    // lower - нижяя граница сортировки
    // higher - права граница сортировки
    public static void sort(int array[], int lower, int higher) {
        // печатаем информацию о сортировке
        LogUtils.log(array, "sort", lower, higher);
        // если у нас верхняя граница стала меньше нижней - останавливаем сортировку (добрались до одного элемента)
        if (higher <= lower) {
            return;
        }
        // вычисляем центральный элемент
        // н-ер: 4 .. 23
        // midle = 4 + (23 - 4) / 2 = 4 + 19 / 2 = 4 + 9 = 13
        int middle = lower + (higher - lower) / 2;
        // как только проваливаемся внутрь - увеличиваем отступ
        LogUtils.indentUp();
        // сортируем левую часть, например от 4 до 13
        sort(array, lower, middle);
        // сортируем правую часть, например от 14 до 23
        sort(array, middle + 1, higher);
        // слияние двух отсортированных подмассивов
        merge(array, lower, middle, higher);
        // как только завершили сортировку этого блока - уменьшаем отступ
        LogUtils.indentDown();
    }

    // слияние двух подмассивов одного массива - нижняя граница, середина, верхняя граница
    private static void merge(int array[], int lower, int middle, int higher) {
        // копируем все сливаемые элементы в вспомогательный массив
        for (int currentIndex = lower; currentIndex <= higher; currentIndex++) {
            helpArray[currentIndex] = array[currentIndex];
        }

        int i = lower, j = middle + 1;
        for (int currentIndex = lower; currentIndex <= higher; currentIndex++) {
            if (i > middle) {
                array[currentIndex] = helpArray[j];
                j++;
            } else if (j > higher) {
                array[currentIndex] = helpArray[i];
                i++;
            } else if (helpArray[j] < helpArray[i]) {
                array[currentIndex] = helpArray[j];
                j++;
            } else {
                array[currentIndex] = helpArray[i];
                i++;
            }
        }
        LogUtils.log(array, "merge", lower, higher);
    }
}