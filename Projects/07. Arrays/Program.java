class Program {
	public static void main(String args[]) {
		// double temperatures[] = new double[8];
		// temperatures[0] = 27.8;
		// temperatures[1] = 13.2;
		// temperatures[2] = 10.5;
		// temperatures[3] = 5.2;
		// temperatures[4] = 10.1;
		// temperatures[5] = 15.1;
		// temperatures[6] = 23.1;
		// temperatures[7] = 18.6;

		double temperatures[] = {27.8, 13.2, 10.5, 5.2, 10.1, 15.1, 23.1, 18.6};

		// int i = 0;
		// while (i < 8) {
		// 	System.out.println(temperatures[i]);
		// 	i++;
		// }
		for (int i = 0; i < temperatures.length; i++) {
			System.out.println(temperatures[i]);
		}

		double sumOfTemperatures = 0;

		for (int i = 0; i < temperatures.length; i++) {
			sumOfTemperatures = sumOfTemperatures + temperatures[i];
		}

		double average = sumOfTemperatures / temperatures.length;

		System.out.println(average);
	}
}