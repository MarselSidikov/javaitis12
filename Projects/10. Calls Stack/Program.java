class Program {

	public static int f(int x, int y, int z) {
		System.out.println("->> In f(" + x + ", " + y + ", " + z + ")");
 		int result = a(x, y, z) - b(x, y, z);
 		System.out.println("<<- From f = " + result);
 		return result;
	}

	public static int a(int x, int y, int z) {
		System.out.println("->> In a(" + x + ", " + y + ", " + z + ")");
 		int result = d(x, y) * e(y, z);
 		System.out.println("<<- From a = " + result);
 		return result;
	}

	public static int b(int x, int y, int z) {
		System.out.println("->> In b(" + x + ", " + y + ", " + z + ")");
 		int result = c(x, y) + d(y, z);
 		System.out.println("<<- From b = " + result);
 		return result;
	}

	public static int c(int x, int y) {
		System.out.println("->> In c(" + x + ", " + y + ")");
 		int result = x / y;
 		System.out.println("<<- From c = " + result);
 		return result;
	}

	public static int d(int x, int y) {
		System.out.println("->> In d(" + x + ", " + y + ")");
 		int result = x - y;
 		System.out.println("<<- From d = " + result);
 		return result;
	}

 	public static int e(int x, int y) {
 		System.out.println("->> In e(" + x + ", " + y + ")");
 		int result = x + y;
 		System.out.println("<<- From e = " + result);
 		return result;
 	}

	public static void main(String[] args) {
		int result = f(16, 20, 15);
		System.out.println(result);
	}
}