class Program {

	public static int sum(int x[]) {
		int result = 0;
		for (int i = 0; i < x.length; i++) {
			result = result + x[i];
		}

		return result;
	}

	public static double average(int x[]) {
		double result = 0;
		for (int i = 0; i < x.length; i++) {
			result = result + x[i];
		}
		result = result / x.length;
		return result;

	}

	public static void main(String[] args) {
		int a[] = {1, 34, 10, 10, 50, 1};
		int b = sum(a);
		int c = average(a);
		System.out.println(b);
	}
}