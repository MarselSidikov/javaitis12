public class Main {

    public static void main(String[] args) {
        Human sasha = new Human();
        Human marsel;
        marsel = new Human(199);
        System.out.println(marsel.km);
        marsel.name = "Marsel".toCharArray();
        marsel.go(100);
        System.out.println(marsel.km);

        Human igor = marsel;
        igor.km = 999;
        System.out.println(marsel.km);
    }
}
