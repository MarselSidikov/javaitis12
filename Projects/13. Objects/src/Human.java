import java.util.Random;

public class Human {
    char name[];
    int km;

    Human() {
        Random random = new Random();
        this.km = random.nextInt(100);
    }

    Human(int km) {
        this.km = km;
    }

    void go(int km) {
        this.km += km;
    }
}
