public class Car {
    private String number;
    private String color;
    private String model;
    private Driver owner;

    public Car(String number, String color, String model, Driver driver) {
        this.number = number;
        this.color = color;
        this.model = model;
        this.owner = driver;
        owner.setCar(this);
    }

    public String getNumber() {
        return number;
    }

    public String getColor() {
        return color;
    }

    public String getModel() {
        return model;
    }

    public void move() {
        System.out.println("Машина поехала");
    }
}
