public class Driver {
    private String name;
    private Car car;

    public Driver(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void drive() {
        car.move();
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
