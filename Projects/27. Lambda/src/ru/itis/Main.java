package ru.itis;

public class Main {

    public static void main(String[] args) {
        String lines[] = {"Hello", "are", "You?", "123"};
        TextProcessor processor = new TextProcessor(lines);

//	    Predicate isUpperFirstLetter = new Predicate() {
//            @Override
//            public boolean test(String line) {
//                return line.charAt(0) >= 'A' && line.charAt(0) <= 'Z';
//            }
//        };

//        Predicate isUpperFirstLetter = line -> {
//            return line.charAt(0) >= 'A' && line.charAt(0) <= 'Z';
//        };

//        Predicate isUpperFirstLetter = line ->
//                line.charAt(0) >= 'A' && line.charAt(0) <= 'Z';

        processor.filter(line -> line.charAt(0) >= 'A' && line.charAt(0) <= 'Z');
        processor.filter(line -> line.charAt(0) == '1');
    }
}
