package ru.itis;

// интерфейс с одним методом называется "функциональным", на основе него можно
// создать лямбда-выражение
public interface Predicate {
    boolean test(String line);
}
