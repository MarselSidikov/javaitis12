package ru.itis;

public class TextProcessor {
    private String lines[];

    public TextProcessor(String lines[]) {
        this.lines = lines;
    }

    public void filter(Predicate predicate) {
        for (int i = 0; i < lines.length; i++) {
            if (predicate.test(lines[i])) {
                System.out.println(lines[i]);
            }
        }
    }
}
