package ru.itis;

public class MainString {

    public static void main(String[] args) {
        // StringPool
	    String s1 = "Hello";
	    String s2 = "Hello";

	    String s3 = new String("Hello");
	    String s4 = new String("Hello");

        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));

        System.out.println(s3 == s4);
        System.out.println(s3.equals(s4));

        String s5 = s1.concat("Bye");
        System.out.println(s5 == s1);

        String s3Interned = s3.intern();
        String s4Interned = s4.intern();
        System.out.println(s1 == s4Interned);
        System.out.println(s1);
        System.out.println(s4Interned);



    }
}
