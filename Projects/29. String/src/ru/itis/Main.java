package ru.itis;

public class Main {
    public static void main(String[] args) {
//        String s1 = "Hello";
//        String s2 = "Bye";
//        String s3 = "Marsel";
//
//        String s4 = s1 + s2;
//        String s5 = s3 + s4;

//        StringBuilder builder = new StringBuilder();
//        StringBuilder builder1 = builder.append("Marsel");
//        StringBuilder builder2 = builder.append("Bye");
//        StringBuilder builder3 = builder.append("Hello");
//
//        System.out.println(builder == builder1);
//        System.out.println(builder1 == builder2);
//        System.out.println(builder2 == builder3);

        StringBuilder builder = new StringBuilder();
        builder.append("Hello")
                .append("Marsel")
                .append("Bye");

//        String result = builder;
          String result = builder.toString();

    }
}
