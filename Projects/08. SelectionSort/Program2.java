import java.util.Arrays;
class Program2 {

	public static void selectionSort(int x[]) {
		for (int i = 0; i < x.length; i++) {
			int currentMinIndex = i;
			int currentMin = x[currentMinIndex];

			for (int j = i + 1; j < x.length; j++) {
				if (x[j] < currentMin) {
					currentMin = x[j];
					currentMinIndex = j;
				}
			}

			int temp = x[i];
			x[i] = x[currentMinIndex];
			x[currentMinIndex] = temp;
		}
	}

	public static void main(String[] args) {
		int a[] = {-13, 20, 15, 2, 1, 3, -18, 10};
		int b[] = {77, 22, 11, 22, 11};
		int c[] = {3, 0, 5, 20, 10};

		selectionSort(a);
		selectionSort(b);
		selectionSort(c);
	
		System.out.println(Arrays.toString(a));
		System.out.println(Arrays.toString(b));
		System.out.println(Arrays.toString(c));

	}
}