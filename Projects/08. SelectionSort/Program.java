import java.util.Arrays;
class Program {
	public static void main(String[] args) {
		int a[] = {-13, 20, 15, 2, 1, 3, -18, 10};

		for (int i = 0; i < a.length; i++) {
			int currentMinIndex = i;
			int currentMin = a[currentMinIndex];

			for (int j = i + 1; j < a.length; j++) {
				if (a[j] < currentMin) {
					currentMin = a[j];
					currentMinIndex = j;
				}
			}

			int temp = a[i];
			a[i] = a[currentMinIndex];
			a[currentMinIndex] = temp;
		}
		/**
		int currentMinIndex = 0;
		int currentMin = a[currentMinIndex];

		for (int j = 1; j < a.length; j++) {
			if (a[j] < currentMin) {
				currentMin = a[j];
				currentMinIndex = j;
			}
		}

		int temp = a[0];
		a[0] = a[currentMinIndex];
		a[currentMinIndex] = temp;
		// --------
		currentMinIndex = 1;
		currentMin = a[currentMinIndex];

		for (int j = 2; j < a.length; j++) {
			if (a[j] < currentMin) {
				currentMin = a[j];
				currentMinIndex = j;
			}
		}

		temp = a[1];
		a[1] = a[currentMinIndex];
		a[currentMinIndex] = temp;
		// --------
		currentMinIndex = 2;
		currentMin = a[currentMinIndex];

		for (int j = 3; j < a.length; j++) {
			if (a[j] < currentMin) {
				currentMin = a[j];
				currentMinIndex = j;
			}
		}

		temp = a[2];
		a[2] = a[currentMinIndex];
		a[currentMinIndex] = temp;
		**/
		System.out.println(Arrays.toString(a));

	}
}