class Program {
	public static void main(String[] args) {
		int a = 17;
		int b = 5;
		double c = a / b;
		System.out.println(c);

		double d = 17.0;
		c = d / b;
		System.out.println(c);

		int x = 5;
		double y = x; // y = 5.0 int -> double

		double z = 13.56;
		// int u = z; compile error
		int u = (int)z; // u = 13
		System.out.println(u); // double -> int 
	}
}