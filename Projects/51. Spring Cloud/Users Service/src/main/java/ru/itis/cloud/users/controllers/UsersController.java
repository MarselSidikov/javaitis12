package ru.itis.cloud.users.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.cloud.users.dto.User;

import java.util.Arrays;
import java.util.List;

@RestController
@Slf4j
public class UsersController {
    @GetMapping("/users")
    public List<User> getUsers() {
        log.info("GET /users");
        return Arrays.asList(
                User.builder()
                        .id(1L)
                        .firstName("Шамиль")
                        .lastName("Гарипов")
                        .build(),
                User.builder()
                        .id(2L)
                        .firstName("Айдар")
                        .lastName("Зарипов")
                        .build(),
                User.builder()
                        .firstName("Михаил")
                        .lastName("Попков")
                        .build());
    }
}
