package ru.itis.cloud.information.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import ru.itis.cloud.information.dto.Course;

import java.util.List;

/**
 * 03.08.2020
 * information-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@FeignClient("courses-service")
public interface CoursesService {
    @GetMapping("/courses")
    List<Course> getCourses();
}
