package ru.itis.cloud.information.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import ru.itis.cloud.information.clients.CoursesService;
import ru.itis.cloud.information.dto.Course;
import ru.itis.cloud.information.dto.Information;
import ru.itis.cloud.information.dto.User;

import java.util.Arrays;

@RestController
@Slf4j
public class InformationController {

//    @Autowired
//    private RestTemplate restTemplate;
//
//    @GetMapping("/information")
//    public Information getInformation() {
//        log.info("GET /information");
//
//        return Information.builder()
//                .courses(Arrays.asList(restTemplate.getForEntity("http://courses-service/courses", Course[].class).getBody()))
//                .users(Arrays.asList(restTemplate.getForEntity("http://users-service/users", User[].class).getBody()))
//                .build();
//    }

    @Autowired
    private CoursesService coursesService;

    @GetMapping("/information")
    public Information getInformation() {
        return Information.builder()
                .courses(coursesService.getCourses())
                .build();
    }
}
