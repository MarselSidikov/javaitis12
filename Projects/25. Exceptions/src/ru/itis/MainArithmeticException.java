package ru.itis;

import java.util.Scanner;

public class MainArithmeticException {

    public static int div(int x, int y) {
        return x / y;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println(div(a, b));
    }
}
