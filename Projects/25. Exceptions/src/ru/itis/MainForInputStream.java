package ru.itis;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class MainForInputStream {
    public static char readFromFile(String fileName) {
        InputStream inputStream = null;
        try {
            System.out.println("Open file");
            inputStream = new FileInputStream(fileName);
            inputStream.skip(10);
            Thread.sleep(20000);
            System.out.println("Read from file");
            return (char)inputStream.read();
        } catch (InterruptedException e) {
            throw new IllegalStateException("Что-то случилось с программой");
        }
        catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Файл не удалось открыть");
        } catch (IOException e) {
            throw new IllegalArgumentException("Какие-то проблемы с чтением файла");
        }
        finally {
            System.out.println("In Finally");
            if (inputStream != null) {
                try {
                    System.out.println("Finally: Закрытие файла");
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Out from Finally");
        }
    }

    public static void main(String[] args) {
        char c = readFromFile("input.txt");
        System.out.println(c);
    }
}
