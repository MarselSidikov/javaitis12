package ru.itis;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

public class MainForOutputStream {
    public static int div(int x, int y) {
        return x / y;
    }

    public static void writeInFile(int number) {
        OutputStream outputStream = null;
        try {
            // преобразуем в строку
            String resultAsString = String.valueOf(number);
            // записываем ее байтовое представление
            outputStream.write(resultAsString.getBytes());
        } catch (IOException e) {
            System.err.println("Ошибка ввода вывода");
            throw new IllegalArgumentException(e);
        } finally { // этот код выполнится даже если все плохо
            if (outputStream != null) {// проверяете что поток есть
                try {
                    // закрываете его
                    outputStream.close();
                } catch (IOException e) {
                    // если и тут все пошло не так просто выводим сообщение об ошибке
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        // создаем объектную переменную для вывода информации
        OutputStream outputStream = null;
        try {
            // создаем файл для вывода
            outputStream = new FileOutputStream("output.txt");
        } catch (FileNotFoundException e) {
            // если какая-то ошибка с файлом, говорим о ней
            System.err.println("Файл открыть не удалось");
        }

        int result = 0;
        result = div(a, b);

        writeInFile(result);
    }
}
