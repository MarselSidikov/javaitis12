package ru.itis.stomp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.TextMessage;

@Controller
public class MessagesController {

    @Autowired
    private SimpMessagingTemplate template;

    // если сообщение с клиента пришло на URL /hello
    @MessageMapping("/hello")
    public void receiveMessageFromClient(Message<String> message) {
        // печатаем это сообщение
        System.out.println(message);
        // отправляем сообщегние на URL /topic/chat, дальше брокер сообщений доставит его
        // всем подписавшимся на этот урл
        template.convertAndSend("/topic/chat", "Hello, " + message.getPayload());
    }

    // если сообщение пришло на URL /bye
    @MessageMapping("/bye")
    @SendTo("/topic/chat") // сразу его отправляем на /topic/chat
    public TextMessage sendToTopic(Message<String> message) {
        return new TextMessage("Bye, " + message.getPayload());
    }
}
