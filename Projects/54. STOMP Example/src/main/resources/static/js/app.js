var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    // создаем сокет, сразу говорим к какому URL-у подключаемся
    var socket = new SockJS('/messages');
    // создаем stomp-клиент
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        // говорим, что подключены
        setConnected(true);
        console.log('Connected: ' + frame);
        // подписываемся на url /topic/chat
        stompClient.subscribe('/topic/chat', function (greeting) {
            showGreeting(greeting);
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    // stompClient.send("/app/hello", {}, $("#name").val());
    stompClient.send("/topic/chat", {}, $("#name").val());
    // stompClient.send("/app/bye", {}, $("#name").val());
}

function showGreeting(message) {
    $("#greetings").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});