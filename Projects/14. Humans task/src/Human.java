public class Human {

    static final int MAX_AGE = 130;
    static final int MIN_AGE = 0;

    private char name[];
    private int age;

    Human(char name[], int age) {
//        this.name = name;
//        this.age = age;
        setAge(age);
        setName(name);
    }

    // метод доступа - сеттер
    void setAge(int age) {
        if (age >= MIN_AGE && age <= MAX_AGE) {
            this.age = age;
        } else {
            age = MIN_AGE;
        }
    }

    int getAge() {
        return age;
    }

    char[] getName() {
        return name;
    }

    void setName(char[] name) {
        this.name = name;
    }
}
