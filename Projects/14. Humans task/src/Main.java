import java.util.Random;

public class Main {

    // вывести самый часто встречаемый возраст людей
    // используем сортировку подсчетом
    // то есть создаем массив, длина которого равна
    // количеству возмжоных значений сортируемой характеристики
    public static void main(String[] args) {
        Random random = new Random();
        String names[] = {"Name1", "Name2", "Name3", "Name4"};

        // массив из 100 объектных переменных
        Human humans[] = new Human[100];

        for (int i = 0; i < humans.length; i++) {
            Human human = new Human(names[random.nextInt(4)].toCharArray(),
                    random.nextInt(Human.MAX_AGE));
            humans[i] = human;
        }

//        humans[58].age = -177;
        humans[58].setAge(-177);
        // например, в 37-м элементе
        // данного массива будет лежать 45
        // это значит, что возраст 37 встретился
        // 45 раз
        int ages[] = new int[Human.MAX_AGE];

        for (int i = 0; i < humans.length; i++) {
            // получили возвраст человека
            // int currentAge = humans[i].age;
            int currentAge = humans[i].getAge();
            ages[currentAge]++;
        }

        int i = 0;



    }
}
