package ru.itis.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.itis.web.models.User;
import ru.itis.web.repositories.UsersRepository;
import ru.itis.web.security.details.UsersDetailsImpl;

/**
 * 30.06.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class UsersController {

    @Autowired
    private UsersRepository usersRepository;

    @GetMapping("/users")
    public String getUsersPage(@AuthenticationPrincipal UsersDetailsImpl usersDetails, Model model) {
        User user = usersDetails.getUser();
        System.out.println(user);
        model.addAttribute("users", usersRepository.findAll());
        return "users_page";
    }
}
