package ru.itis.web.forms;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import ru.itis.web.validation.NameAndSurnameBadMatch;
import ru.itis.web.validation.ValidPassword;

/**
 * 01.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@NameAndSurnameBadMatch(
        message = "{errors.incorrect.NameAndSurname}",
        name =  "firstName",
        surname = "lastName"
)
public class UserForm {

    @Email(message = "{errors.incorrect.email}")
    private String email;

    @NotBlank(message = "{errors.blank.password}")
    @ValidPassword(message = "{errors.incorrect.password}")
    private String password;

    private String firstName;
    private String lastName;
}
