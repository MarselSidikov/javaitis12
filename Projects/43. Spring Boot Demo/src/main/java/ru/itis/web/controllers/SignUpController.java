package ru.itis.web.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.web.forms.UserForm;
import ru.itis.web.services.SignUpService;

import javax.validation.Valid;
import java.util.Objects;

/**
 * 01.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Slf4j
@Controller
public class SignUpController {

    @Autowired
    private SignUpService signUpService;

    @PostMapping("/signUp")
    public String signUpUser(@Valid UserForm userForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            if (bindingResult.getAllErrors()
                    .stream().anyMatch(error -> Objects.requireNonNull(error.getCodes())[0]
                            .equals("userForm.NameAndSurnameBadMatch"))) {
                model.addAttribute("namesError", new Object());
            }
            model.addAttribute("userForm", userForm);
            return "sign_up_page";
        }
        signUpService.signUp(userForm);
        return "redirect:/signIn";
    }

    @GetMapping("/signUp")
    public String getSignUpPage(Authentication authentication, Model model) {
        if (authentication == null) {
            model.addAttribute("userForm", new UserForm());
            return "sign_up_page";
        } else {
            return "redirect:/";
        }
    }
}
