package ru.itis.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.annotation.security.DenyAll;

/**
 * 11.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CountryDto {
    @JsonProperty(value = "Country")
    private String country;

    @JsonProperty(value = "NewConfirmed")
    private Long newConfirmed;

    @JsonProperty(value = "TotalConfirmed")
    private Long totalConfirmed;
}
