package ru.itis.web.services;

import ru.itis.web.dto.CountriesDto;
import ru.itis.web.dto.CountryDto;

import java.util.List;

/**
 * 11.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CovidService {
    List<CountryDto> getSummary();
}
