package ru.itis.web.models;

/**
 * 09.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public enum State {
    NOT_CONFIRMED, CONFIRMED
}
