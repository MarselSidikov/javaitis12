package ru.itis.web.services;

import ru.itis.web.forms.UserForm;

/**
 * 01.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SignUpService {
    void signUp(UserForm userForm);
}
