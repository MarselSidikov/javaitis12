package ru.itis.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.itis.web.dto.CountriesDto;
import ru.itis.web.dto.CountryDto;

import java.util.Arrays;
import java.util.List;

/**
 * 11.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class CovidServiceImpl implements CovidService {

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<CountryDto> getSummary() {
        return Arrays.asList(restTemplate.getForObject("https://api.covid19api.com/summary", CountriesDto.class).getCountries());
    }
}
