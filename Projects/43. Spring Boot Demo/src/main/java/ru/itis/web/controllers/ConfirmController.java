package ru.itis.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.itis.web.services.ConfirmService;

/**
 * 09.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class ConfirmController {

    @Autowired
    private ConfirmService confirmService;

    @GetMapping("/users/confirm/{confirm-code}")
    public String getConfirmPage(Model model, @PathVariable("confirm-code") String confirmCode) {
        boolean isConfirmed = confirmService.confirmUser(confirmCode);
        model.addAttribute("isConfirmed", isConfirmed);
        return "success_confirm_page";
    }
}
