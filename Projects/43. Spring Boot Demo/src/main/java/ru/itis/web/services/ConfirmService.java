package ru.itis.web.services;

/**
 * 09.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ConfirmService {
    boolean confirmUser(String confirmCode);
}
