package ru.itis.web.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 01.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class SignInController {
    @GetMapping("/signIn")
    public String getSignInPage(Authentication authentication) {
        if (authentication == null) {
            return "sign_in_page";
        } else {
            return "redirect:/";
        }
    }
}
