package ru.itis.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.itis.web.services.CovidService;

/**
 * 11.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class CovidController {

    @Autowired
    private CovidService covidService;

    @GetMapping("/covid/summary")
    public String getCovidInformation(Model model) {
        model.addAttribute("countries", covidService.getSummary());
        return "covid_statistic";
    }
}
