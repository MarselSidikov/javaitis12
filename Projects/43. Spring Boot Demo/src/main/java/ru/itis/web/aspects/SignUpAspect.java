package ru.itis.web.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 24.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Aspect
@Component
public class SignUpAspect {
    private Logger logger =LoggerFactory.getLogger(SignUpAspect.class);

    @Before(value = "execution(* ru.itis.web.services.SignUpService.signUp(*))")
    public void before(JoinPoint joinPoint) {
        Object args[] = joinPoint.getArgs();

        logger.info("Пользователь " + args[0] + " начал регистрацию");
    }

    @Before(value = "execution(public * org.springframework.mail.javamail.JavaMailSender.send(*))")
    public void beforeSendingEmail(JoinPoint joinPoint) {
        logger.info("Sending message " + joinPoint.getArgs()[0]);
    }

    @AfterReturning(value = "execution(public * ru.itis.web.services.ConfirmService.confirmUser(*))",
            returning = "isSuccessful")
    public void afterConfirmed(JoinPoint joinPoint, Boolean isSuccessful) {
        if (!isSuccessful) {
            logger.error(joinPoint.getArgs()[0] + " is bad code");
        }
    }
}
