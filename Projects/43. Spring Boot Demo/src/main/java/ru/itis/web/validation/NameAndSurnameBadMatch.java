package ru.itis.web.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = NameAndSurnameValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface NameAndSurnameBadMatch {
 
    String message() default "Fields values match!";
 
    String name();
 
    String surname();
 
    @Target({ ElementType.TYPE })
    @Retention(RetentionPolicy.RUNTIME)
    @interface List {
        NameAndSurnameBadMatch[] value();
    }

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}