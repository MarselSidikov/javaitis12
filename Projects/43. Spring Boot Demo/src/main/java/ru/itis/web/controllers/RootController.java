package ru.itis.web.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 02.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
@RequestMapping("/")
public class RootController {

    @Value("${application.root.redirect}")
    private String redirectUrl;

    @GetMapping
    public String getRootPage(Authentication authentication) {
        if (authentication == null) {
            return "redirect:/signIn";
        } else {
            return "redirect:" + redirectUrl;
        }
    }
}
