package ru.itis.web.validation;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 18.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NameAndSurnameValidator implements ConstraintValidator<NameAndSurnameBadMatch, Object> {

    private String namePropertyName;
    private String surnamePropertyName;

    @Override
    public void initialize(NameAndSurnameBadMatch constraintAnnotation) {
        this.namePropertyName = constraintAnnotation.name();
        this.surnamePropertyName = constraintAnnotation.surname();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        Object name = new BeanWrapperImpl(value)
                .getPropertyValue(namePropertyName);
        Object surname = new BeanWrapperImpl(value)
                .getPropertyValue(surnamePropertyName);

        return name != null && !name.equals(surname);
    }
}
