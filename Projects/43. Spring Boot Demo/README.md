* username = marsel
 * password = qwerty007
 
 * hash = $2a$10$FTo9JSjnnoO1YHsxH7jOk.RQumnz/YojMXAiq4z.Kp/mgZ.ZmVWi.
 
 1. Пользователь вводит логин и пароль
 2. Мы находим пользователя с этим логином в базе данных
 
 * qwerty007 ~ $2a$10$FTo9JSjnnoO1YHsxH7jOk.RQumnz/YojMXAiq4z.Kp/mgZ.ZmVWi.
 
 * hash(salt.password) = hash(FTo9JSjnnoO1YHsxH7jOk.qwerty007) = RQumnz/YojMXAiq4z.Kp/mgZ.ZmVWi. 
 
 * $2a$10$salt.hash(salt.password) = $2a$10$FTo9JSjnnoO1YHsxH7jOk.RQumnz/YojMXAiq4z.Kp/mgZ.ZmVWi.
 
 * Проверка -> hash(FTo9JSjnnoO1YHsxH7jOkqwerty007) ~ RQumnz/YojMXAiq4z.Kp/mgZ.ZmVWi.