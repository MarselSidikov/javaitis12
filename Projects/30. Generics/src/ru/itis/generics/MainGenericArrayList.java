package ru.itis.generics;

public class MainGenericArrayList {
    public static void main(String[] args) {
        GenericArrayList<Integer> integersList = new GenericArrayList<>();
        integersList.add(15);
        integersList.add(20);
        integersList.add(33);

        int firstInteger = integersList.get(0);
        int secondInteger = integersList.get(1);
//        integersList.add("Hello");

        GenericArrayList<String> stringsList = new GenericArrayList<>();
        stringsList.add("Hello");
        stringsList.add("Bye");
        stringsList.add("Marsel");
        String firstWord = stringsList.get(0);
        String secondWord = stringsList.get(1);
//        stringsList.add(15);

        GenericArrayList someList = new GenericArrayList();
        someList.add("Hello");
        someList.add(15);
        String a = (String) someList.get(0);
        int b = (int) someList.get(1);
    }
}
