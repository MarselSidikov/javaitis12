package ru.itis.generics;

public interface List<V> {
    V get(int index);
    void add(V object);
}
