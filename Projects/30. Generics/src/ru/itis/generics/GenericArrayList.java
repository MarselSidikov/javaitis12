package ru.itis.generics;

public class GenericArrayList<T> implements List<T> {
    private static final int DEFAULT_ARRAY_SIZE = 10;

    private Object elements[];
    private int count;

    public GenericArrayList() {
        this.elements = new Object[DEFAULT_ARRAY_SIZE];
    }

    public void add(T element) {
        this.elements[count] = element;
        count++;
    }

    public T get(int index) {
        return (T)elements[index];
    }
}
