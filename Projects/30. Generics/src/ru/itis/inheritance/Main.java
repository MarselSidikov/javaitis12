package ru.itis.inheritance;

public class Main {
    public static void main(String[] args) {
        Generic<A> aGeneric = new Generic<>();
        Generic<B> bGeneric = new Generic<>();
        Generic<Object> objectGeneric = new Generic<>();
        Generic generic = new Generic();

        // aGeneric = bGeneric;
        // objectGeneric = aGeneric;
        generic = aGeneric;
        generic = bGeneric;
        generic = objectGeneric;

    }
}
