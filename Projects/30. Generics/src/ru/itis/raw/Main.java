package ru.itis.raw;

public class Main {

    public static void main(String[] args) {
	    IntegersArrayList integersList = new IntegersArrayList();
	    integersList.add(10);
	    integersList.add(15);
	    integersList.add(30);
        System.out.println(integersList.get(0));
        System.out.println(integersList.get(1));
        System.out.println(integersList.get(2));

        StringsArrayList stringsList = new StringsArrayList();
        stringsList.add("Hello");
        stringsList.add("Bye");
        stringsList.add("Marsel");
        System.out.println(stringsList.get(0));
        System.out.println(stringsList.get(1));
        System.out.println(stringsList.get(2));
    }
}
