package ru.itis.objects;

public class MainWrappers {
    public static void main(String[] args) {
        // boxing - упаковка примтивный тип упаковали в оберточный
        Integer i1 = new Integer(1);
        // unboxing - распаковка, из ссылочного типа значение кладем в примитивный
        int i2 = i1.intValue();

        // autoboxing
        Integer i3 = 10;
        // autounboxing
        int i4 = i3;

    }
}
