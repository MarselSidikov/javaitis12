package ru.itis.objects;

public class ObjectsArrayList {
    private static final int DEFAULT_ARRAY_SIZE = 10;

    private Object elements[];
    private int count;

    public ObjectsArrayList() {
        this.elements = new Object[DEFAULT_ARRAY_SIZE];
    }

    public void add(Object element) {
        this.elements[count] = element;
        count++;
    }

    public Object get(int index) {
        return elements[index];
    }


}
