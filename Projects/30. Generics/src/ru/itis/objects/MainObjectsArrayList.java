package ru.itis.objects;

public class MainObjectsArrayList {
    public static void main(String[] args) {
        ObjectsArrayList integersList = new ObjectsArrayList();
        integersList.add(10);
        integersList.add(15);
        integersList.add(30);
        // Object -> int
        int firstInteger = (int)integersList.get(0);
        int secondInteger = (int)integersList.get(1);
        int thirdInteger = (int)integersList.get(2);
        System.out.println(firstInteger);
        System.out.println(secondInteger);
        System.out.println(thirdInteger);

        ObjectsArrayList stringsList = new ObjectsArrayList();
        stringsList.add("Hello");
        stringsList.add("Bye");
        stringsList.add("Marsel");
        // Object -> String
        String firstWord = (String)stringsList.get(0);
        String secondWord = (String)stringsList.get(1);
        String thirdWord = (String)stringsList.get(2);
        System.out.println(firstWord);
        System.out.println(secondWord);
        System.out.println(thirdWord);

        //----------------------------
        stringsList.add(15);
        integersList.add("Hello");

        String lastWord = (String) stringsList.get(3);
        int lastInteger = (int)integersList.get(3);
    }
}
