public class SequenceSelectionSortImpl extends AbstractSequence {

    public SequenceSelectionSortImpl(int[] sequence) {
        super(sequence);
    }

    @Override
    public void sort() {
        for (int i = 0; i < sequence.length; i++) {
            int min = sequence[i];
            int minIndex = i;
            for (int j = i; j < sequence.length; j++) {
                if (min > sequence[j]) {
                    min = sequence[j];
                    minIndex = j;
                }
            }
            int temp = sequence[i];
            sequence[i] = sequence[minIndex];
            sequence[minIndex] = temp;
        }
    }
}
