public class SequenceBubbleSortImpl extends AbstractSequence {
    public SequenceBubbleSortImpl(int[] sequence) {
        super(sequence);
    }

    @Override
    public void sort() {
        for (int i = sequence.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (sequence[j] > sequence[j + 1]) {
                    int temp = sequence[j];
                    sequence[j] = sequence[j + 1];
                    sequence[j + 1] = temp;
                }
            }
        }
    }
}
