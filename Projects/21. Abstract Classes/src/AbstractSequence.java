public abstract class AbstractSequence {
    protected int sequence[];
    private boolean isSorted;

    public AbstractSequence(int sequence[]) {
        this.sequence = new int[sequence.length];
        for (int i = 0; i < sequence.length; i++) {
            this.sequence[i] = sequence[i];
        }
        this.isSorted = false;
    }

    public int min() {
        if (!isSorted) {
            sort();
            isSorted = true;
        }
        return sequence[0];
    }

    public int max() {
        if (!isSorted) {
            sort();
            isSorted = true;
        }
        return sequence[sequence.length  - 1];
    }

    public abstract void sort();
}
