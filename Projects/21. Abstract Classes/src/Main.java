public class Main {

    public static void main(String[] args) {
        int array[] = {3, 2, 1, 0, 10, 1, -10};
//        AbstractSequence sequence = new SequenceBubbleSortImpl(array);
        AbstractSequence sequence = new SequenceSelectionSortImpl(array);
        System.out.println(sequence.min());
        System.out.println(sequence.max());
    }
}
