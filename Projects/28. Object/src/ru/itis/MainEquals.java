package ru.itis;

public class MainEquals {
    public static void main(String[] args) {
        User user = new User("Марсель", 26, 1.85);
        User user1 = new User("Марсель", 26, 1.85);

//        System.out.println(user == user1);
//        System.out.println(user.equals(user1));

        Object o = user;
        Object o1 = user1;

        System.out.println(o.equals(o1));
    }
}
