package ru.itis;

public class User {
    private String name;
    private int age;
    private double height;

    public User(String name, int age, double height) {
        this.name = name;
        this.age = age;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

//    public boolean equals(User user) {
//        System.out.println("Bad equals used");
//        return true;
//    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }
        if (!(object instanceof User)) {
            return false;
        }

        User that = (User)object;

        return this.age == that.age
                && Double.compare(this.height, that.height) == 0
                && this.name.equals(that.name);
    }

    @Override
    public String toString() {
        return "User { " + this.name + " " + this.age + " " + this.height + "}";
    }
}
