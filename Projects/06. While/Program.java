import java.util.Scanner;
class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int currentNumber;
		currentNumber = scanner.nextInt();

		int oddNumbersCount = 0;
		int evenNumbersCount = 0;

		while (currentNumber != -1) {
			if (currentNumber % 2 == 0) {
				//evenNumbersCount = evenNumbersCount + 1;
				evenNumbersCount++;
			} else {
				// oddNumbersCount = oddNumbersCount + 1;
				oddNumbersCount++;
			}
			currentNumber = scanner.nextInt();
		}

		System.out.println("Odd count: " + oddNumbersCount);
		System.out.println("Even count: " + evenNumbersCount);
	}
}