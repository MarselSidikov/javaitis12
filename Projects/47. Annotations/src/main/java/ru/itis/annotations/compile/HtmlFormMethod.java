package ru.itis.annotations.compile;

/**
 * 15.07.2020
 * 47. Annotations
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public enum HtmlFormMethod {
    GET, POST
}
