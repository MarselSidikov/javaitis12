package ru.itis.annotations.runtime;

import javax.sql.DataSource;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * 16.07.2020
 * 47. Annotations
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SqlTablesGenerator {

    private final static Map<String, String> SQL_TYPES = new HashMap<>();

    static {
        SQL_TYPES.put("Integer", "integer");
        SQL_TYPES.put("Boolean", "boolean");
        SQL_TYPES.put("Long", "bigint");
    }

    private DataSource dataSource;

    public SqlTablesGenerator(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> void generateSQL(Class<T> modelClass) {
        try {
            generateSQL0(modelClass);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private <T> void generateSQL0(Class<T> modelClass) throws Exception {
        Model modelClassAnnotation =  modelClass.getAnnotation(Model.class);
        String tableName = modelClassAnnotation.tableName();

        Map<String, String> columns = new HashMap<>();

        for (Field field : modelClass.getDeclaredFields()) {
            String fieldType = field.getType().getSimpleName();

            Id idFieldAnnotation = field.getAnnotation(Id.class);

            if (idFieldAnnotation != null) {
                if (fieldType.equals("Long")) {
                    columns.put("id", "bigserial primary key");
                } else if (fieldType.equals("Integer")) {
                    columns.put("id", "serial primary key");
                }
            } else {
                Column columnAnnotation = field.getAnnotation(Column.class);

                if (fieldType.equals("String")) {
                    columns.put(columnAnnotation.name(), "varchar(" + columnAnnotation.length() + ")");
                } else {
                    columns.put(columnAnnotation.name(), SQL_TYPES.get(fieldType));
                }
            }
        }

        Connection connection = dataSource.getConnection();

        Statement statement = connection.createStatement();

        StringBuilder query = new StringBuilder();

        query.append("create table ").append(tableName).append(" ( \n");

        for (Map.Entry<String, String> column : columns.entrySet()) {
            query.append(column.getKey()).append(" ").append(column.getValue()).append(", \n");
        }
        query.deleteCharAt(query.toString().lastIndexOf(","));
        query.append(");");

        System.out.println(query.toString());

        statement.execute(query.toString());

        statement.close();
        connection.close();
    }
}
