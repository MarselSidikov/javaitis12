package ru.itis.annotations.runtime;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * 16.07.2020
 * 47. Annotations
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

/*
spring.datasource.url=jdbc:postgresql://localhost:5432/education_center
spring.datasource.username=postgres
spring.datasource.password=qwerty007
 */
public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/education_center",
                "postgres", "qwerty007");

        SqlTablesGenerator generator = new SqlTablesGenerator(dataSource);
        generator.generateSQL(Course.class);
    }
}
