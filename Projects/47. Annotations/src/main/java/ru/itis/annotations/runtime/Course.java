package ru.itis.annotations.runtime;

/**
 * 16.07.2020
 * 47. Annotations
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Model(tableName = "course_table")
public class Course {
    @Id
    private Long id;

    @Column(length = 20, name = "title_of_course")
    private String title;
    @Column(name = "duration_of_course")
    private Integer duration;
    @Column(name = "is_active")
    private Boolean isActive;
}
