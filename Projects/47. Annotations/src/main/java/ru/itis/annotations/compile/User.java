package ru.itis.annotations.compile;

/**
 * 15.07.2020
 * 47. Annotations
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@HtmlForm(method = HtmlFormMethod.GET, fileName = "user_form.html", action = "http://localhost/users")
public class User {
    @HtmlInput(name = "login")
    private String login;
    @HtmlInput(type = "password", name = "password")
    private String password;
}
