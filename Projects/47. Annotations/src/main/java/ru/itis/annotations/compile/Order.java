package ru.itis.annotations.compile;

/**
 * 15.07.2020
 * 47. Annotations
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@HtmlForm(method = HtmlFormMethod.POST, fileName = "order_form.html", action = "http://localhost/orders")
public class Order {
    @HtmlInput(name = "name")
    private String name;
    @HtmlInput(name = "description")
    private String description;
}
