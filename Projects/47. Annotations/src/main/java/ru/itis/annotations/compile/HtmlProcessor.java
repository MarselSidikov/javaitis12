package ru.itis.annotations.compile;

import com.google.auto.service.AutoService;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

/**
 * 10.07.2020
 * anno
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@AutoService(Processor.class)
@SupportedAnnotationTypes(value = {"ru.itis.annotations.compile.HtmlForm", "ru.itis.annotations.compile.HtmlInput"})
public class HtmlProcessor extends AbstractProcessor {


    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        // получаем все классы, которые помечены аннотацией HtmlForm
        Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(HtmlForm.class);

        // проходим по каждому классу
        for (Element element : annotatedElements) {
            HtmlForm annotation = element.getAnnotation(HtmlForm.class);
            // сформировать файл, в который я хочу писать что-то html - страницу
            Path out = Paths.get(annotation.fileName());
            try {
                // открываем файл для записи
                BufferedWriter writer = new BufferedWriter(new FileWriter(out.toFile()));
                String action = annotation.action();
                String method = annotation.method().toString();
                // создаем форму
                writer.write("<form action='" + action + "' method = '" + method + "'>");
                writer.close();
            } catch (IOException e) {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
            }
        }

        return true;
    }
}

