```java
@HtmlForm(fileName = "user_form.html", action = "/users", method = "post")
class User {
    @HtmlInput(type = "text", name = "first_name")
    private String firstName;
    private String lastName;
    @HtmlInput(type = "password", name = "password")
    private String password;
}
```

* `user_form.html`

```html
<form action = "/users" method="post">
    <input type="text" name = "first_name">
    <input type="password" name="password">
    <input type="submit">
</form> 
```