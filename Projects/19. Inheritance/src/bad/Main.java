package bad;

public class Main {

    public static void main(String[] args) {
        Player player = new Player(0, 100);
        player.hitEnemy();
        player.hitEnemy();
        player.damageTaken();

        System.out.println(player.getHealth() + " " + player.getScore());
    }
}
