package bad;

public class UpgradedPlayer {
    private int score;
    private int health;
    private int bulletsCount;

    public UpgradedPlayer(int score, int health, int bulletsCount) {
        if (score >= 0) {
            this.score = score;
        }
        if (health >= 0) {
            this.health = health;
        }
        if (bulletsCount >= 0) {
            this.bulletsCount = bulletsCount;
        }
    }

    public void hitEnemy() {
        this.score += 10;
        this.bulletsCount--;
    }

    public void damageTaken() {
        this.health -= 3;
    }

    public int getScore() {
        return score;
    }

    public int getHealth() {
        return health;
    }

    public int getBulletsCount() {
        return bulletsCount;
    }
}
