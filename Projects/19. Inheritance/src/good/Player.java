package good;

public class Player {
    protected int score;
    protected int health;


    public Player(int score, int health) {
        if (score >= 0) {
            this.score = score;
        }
        if (health >= 0) {
            this.health = health;
        }
    }

    public void hitEnemy() {
        this.score += 5;
    }

    public void damageTaken() {
        this.health -= 5;
    }

    public void sayHello() {
        System.out.println("Hello!");
    }

    public int getScore() {
        return score;
    }

    public int getHealth() {
        return health;
    }
}
