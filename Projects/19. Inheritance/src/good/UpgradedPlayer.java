package good;

public class UpgradedPlayer extends Player {
    private int bulletsCount;

    public UpgradedPlayer(int score, int health, int bulletsCount) {
        // вызов конструктора предка
        super(score, health);
        if (bulletsCount >= 0) {
            this.bulletsCount = bulletsCount;
        }
    }

    @Override
    public void hitEnemy() {
        this.score += 10;
        this.bulletsCount--;
    }

    @Override
    public void damageTaken() {
        this.health -= 3;
    }


    public int getBulletsCount() {
        return bulletsCount;
    }
}
