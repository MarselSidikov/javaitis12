package good;

public class Main {
    public static void main(String[] args) {
        UpgradedPlayer upgradedPlayer =
                new UpgradedPlayer(0, 100, 100);

        Player player = upgradedPlayer;

        upgradedPlayer.sayHello();

        player.damageTaken();

        System.out.println(player.getHealth());

//        player = new Player(10, 10);

        int i = ((UpgradedPlayer)player).getBulletsCount();
        System.out.println(i);
    }
}
