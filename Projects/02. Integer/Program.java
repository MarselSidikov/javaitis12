class Program {
	public static void main(String[] args) {
		int a = 5;
		a = a + 1;
		System.out.println(a);

		int number = 2147483647;
		number = number + 1;
		System.out.println(number);	

		// X = Y * Z + D
		// 17 = 5 * 3 + 2
		int x = 17;
		int y = 5;
		int z = x / y;
		int d = x % z;
		System.out.println(z);
		System.out.println(d);

	}
}