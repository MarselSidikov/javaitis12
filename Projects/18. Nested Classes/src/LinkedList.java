public class LinkedList {
    Node first;
    private Node last;

    private static int staticvalue;

    // nested (static nested)
    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

    // inner (non-static nested)
    class LinkedListIterator {
        Node current;

        public LinkedListIterator() {
            this.current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public int next() {
            int value = current.value;
            current = current.next;
            return value;
        }
    }

    private int count;

    public LinkedList() {

    }

    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }

    public LinkedListIterator iterator() {
        return new LinkedListIterator();
    }
}
