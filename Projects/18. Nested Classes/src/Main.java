public class Main {

    public static void main(String[] args) {
//	    LinkedList.Node node = new LinkedList.Node(10);

        // не могу создать объект внутреннего класса без объекта внешнего
        // LinkedList.LinkedListIterator iterator = new LinkedList.LinkedListIterator();
        LinkedList list = new LinkedList();

        list.add(10);
        list.add(15);
        list.add(20);
//        LinkedList.LinkedListIterator iterator = list. new LinkedListIterator();

        LinkedList.LinkedListIterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

//        LinkedList.LinkedListIterator iterator2 = list. new LinkedListIterator();
        LinkedList.LinkedListIterator iterator2 = list.iterator();

        while (iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }
    }
}
