package ru.itis;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.InputStream;

public class MainReadAsCharArray {
    public static void main(String[] args) throws Exception {
        InputStream input = new FileInputStream("input.txt");

        byte text[] = new byte[100];
        int code;
        int i = 0;
        while ((code = input.read()) != -1) {
            text[i] =(byte) code;
            i++;
        }
        String line = new String(text, 0, i);
        System.out.println(line);
    }
}
