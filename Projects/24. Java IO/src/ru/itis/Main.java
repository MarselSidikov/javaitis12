package ru.itis;

import java.io.FileInputStream;
import java.io.InputStream;
import java.time.LocalTime;

public class Main {

    public static void main(String[] args) throws Exception {
        InputStream input = new FileInputStream("input.txt");
        int a = input.read();
        int b = input.read();
        int c = input.read();
        int d = input.read();
        int e = input.read();
        int f = input.read();
        System.out.println((char)a);
        System.out.println((char)b);
        System.out.println((char)c);
        System.out.println((char)d);
        System.out.println((char)e);
        System.out.println((char)f);
        input.close();
    }
}
