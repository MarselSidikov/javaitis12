package ru.itis.chat.dto;

import lombok.Data;

/**
 * 05.08.2020
 * 52. Long Polling
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
public class MessageDto {
    private String text;
}
