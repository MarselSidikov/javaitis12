package ru.itis.chat;

import ru.itis.chat.dto.MessageDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 05.08.2020
 * 52. Long Polling
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Messages {
    public static final Map<String, List<MessageDto>> messages = new HashMap<>();
}
