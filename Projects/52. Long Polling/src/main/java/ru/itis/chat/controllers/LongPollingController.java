package ru.itis.chat.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itis.chat.Messages;
import ru.itis.chat.dto.MessageDto;

import java.sql.ResultSet;
import java.util.*;

/**
 * 05.08.2020
 * 52. Long Polling
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
public class LongPollingController {

    // запрашиваем сообщения конкретного пользователя
    @GetMapping("/messages")
    public ResponseEntity<List<MessageDto>> getMessages(@RequestParam("userId") String userId) {
        // бесконечный цикл
        while (true) {
            // если сообщений нет для конкретного userId
            synchronized (Messages.messages.get(userId)) {
                if (Messages.messages.get(userId).isEmpty()) {
                    try {
                        // уходим в ожидание
                        Messages.messages.get(userId).wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }
                // получаем список всех сообщений для этого пользователя
                List<MessageDto> messagesForCurrent = new ArrayList<>(Messages.messages.get(userId));
                // очищаем исходный список
                Messages.messages.get(userId).clear();
                // возвращаем результат
                return ResponseEntity.ok(messagesForCurrent);
            }
        }

    }

    @PostMapping("/messages")
    public ResponseEntity<Object> addMessage(@RequestBody MessageDto message) {
        synchronized (Messages.messages) {
            for (String userId : Messages.messages.keySet()) {
                // положили сообщение пользователю
                synchronized (Messages.messages.get(userId)) {
                    Messages.messages.get(userId).add(message);
                    // оповестили ожидающий поток
                    Messages.messages.get(userId).notify();
                }
            }
        }
        return ResponseEntity.ok().build();
    }
}
