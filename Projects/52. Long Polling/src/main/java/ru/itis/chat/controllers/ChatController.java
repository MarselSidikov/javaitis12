package ru.itis.chat.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.itis.chat.Messages;

import java.util.ArrayList;
import java.util.UUID;

/**
 * 05.08.2020
 * 52. Long Polling
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class ChatController {
    @GetMapping("/chat")
    public String getMessagesPage(Model model) {
        // блокируем очередь сообщений для текущего пользователя
        synchronized (Messages.messages) {
            // генерируем идентификатор этому пользователю
            String userId = UUID.randomUUID().toString();
            // кладем пустой список сообщений для этого пользователя
            Messages.messages.put(userId, new ArrayList<>());
            // кладем идентификатор на страницу
            model.addAttribute("id", userId);
        }

        return "messages";
    }
}
