* ORM - Object Relational Mapping - механизм, с помощью которого объектно-ориентированные отношения отображаются в реляционные отношения.

* JPA - Java Persistence API - набор интерфейсов и аннотаций, которые задают СТАНДАРТ ORM в Java

* Hibernate - ORM-фреймворк, реализация JPA

* Транзакция - операция в БД, которая выполняется либо полностью, либо вообзе не выполняется

* [HQL](https://www.tutorialspoint.com/hibernate/hibernate_query_language.htm)

