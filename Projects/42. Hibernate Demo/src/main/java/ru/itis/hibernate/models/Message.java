package ru.itis.hibernate.models;

import lombok.*;

/**
 * 19.06.2020
 * 42. Hibernate Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = {"author", "room"})
@EqualsAndHashCode(exclude = {"author", "room"})
public class Message {

    private Long id;

    private String text;

    private User author;

    private Room room;
}
