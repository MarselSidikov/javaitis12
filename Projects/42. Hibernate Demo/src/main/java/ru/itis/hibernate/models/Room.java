package ru.itis.hibernate.models;

import lombok.*;

import java.util.Set;

/**
 * 19.06.2020
 * 42. Hibernate Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = {"creator", "users"})
@EqualsAndHashCode(exclude = {"creator", "users"})
public class Room {

    private Long id;

    private User creator;

    private String name;

    private Set<User> users;

    private Set<Message> messages;
}
