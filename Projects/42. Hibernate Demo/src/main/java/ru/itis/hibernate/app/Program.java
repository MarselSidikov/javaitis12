package ru.itis.hibernate.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.itis.hibernate.models.User;

/**
 * 19.06.2020
 * 42. Hibernate Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program {
    public static void main(String[] args) {
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml_");
        // Фабричный класс - создает сессии, аналог Connection в JDBC
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        // открываем сессию
        Session session = sessionFactory.openSession();
        // начинаем транзакцию
        session.beginTransaction();
        // создаем объект

        User user = User.builder()
                .username("ALPHA")
                .build();
        // user -> TRANSIENT
        System.out.println("->> SESSION PERSIST");
        session.persist(user);
        // user -> MANAGED
        System.out.println("->> UPDATE USERNAME");
        user.setUsername("BETA");
        System.out.println("->> TRANSACTION COMMIT");
        session.getTransaction().commit();
        session.close();
        // user -> DETACHED
        user.setUsername("OMEGA");
        sessionFactory.close();

        configuration = new Configuration().configure("hibernate.cfg.xml_");
        sessionFactory = configuration.buildSessionFactory();

        session = sessionFactory.openSession();
        session.beginTransaction();
        System.out.println("->> SESSION MERGE");
        System.out.println("BEFORE MERGE: " + user);
        session.merge(user);
        System.out.println("AFTER MERGE: " + user);
        // MANAGED
        System.out.println("->> TRANSACTION COMMIT");
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
    }
}
