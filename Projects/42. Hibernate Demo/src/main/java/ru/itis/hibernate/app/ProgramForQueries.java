package ru.itis.hibernate.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.itis.hibernate.models.Message;
import ru.itis.hibernate.models.Room;
import ru.itis.hibernate.models.User;

import java.util.List;

/**
 * 22.06.2020
 * 42. Hibernate Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ProgramForQueries {
    public static void main(String[] args) {
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml_");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();
        Query<User> userQuery = session.createQuery("from User where id = 1", User.class);
        User user = userQuery.getSingleResult();

        Room room = session.get(Room.class, 5L);

        Query<Message> messageQuery = session.createQuery("from Message message where message.author = :author and message.room = :room", Message.class)
                .setParameter("author", user)
                .setParameter("room", room);

        List<Message> messages = messageQuery.getResultList();

        System.out.println(messages);
        session.close();
        sessionFactory.close();
    }
}
