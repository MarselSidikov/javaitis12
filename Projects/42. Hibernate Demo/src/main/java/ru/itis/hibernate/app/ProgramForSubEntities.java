package ru.itis.hibernate.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import ru.itis.hibernate.models.Message;
import ru.itis.hibernate.models.Room;
import ru.itis.hibernate.models.User;

/**
 * 20.06.2020
 * 42. Hibernate Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ProgramForSubEntities {
    public static void main(String[] args) {
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml_");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query<User> userQuery = session.createQuery("from User where id = 1", User.class);
        User user = userQuery.getSingleResult();

        Room room = Room.builder()
                .creator(user)
                .name("FIRST ROOM")
                .build();

        Message message = Message.builder()
                .author(user)
                .text("Hello!")
                .room(room)
                .build();

        user.getRooms().add(room);
        session.save(room);
        session.save(message);
        session.getTransaction().commit();
        session.close();

        session = sessionFactory.openSession();
        userQuery = session.createQuery("from User where id = 1", User.class);
        user = userQuery.getSingleResult();
        System.out.println(user);
        session.close();


        sessionFactory.close();
    }
}
