package ru.itis.jpa.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

/**
 * 19.06.2020
 * 42. Hibernate Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "account")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    @ManyToMany(mappedBy = "users")
    private Set<Room> rooms;

    @OneToMany(mappedBy = "author")
    private Set<Message> messages;

    @OneToMany(mappedBy = "creator")
    private Set<Room> createdRooms;

}
