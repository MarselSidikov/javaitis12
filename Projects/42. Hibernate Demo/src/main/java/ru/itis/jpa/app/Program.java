package ru.itis.jpa.app;

import ru.itis.jpa.models.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.transaction.Transaction;
import java.util.Map;

/**
 * 22.06.2020
 * 42. Hibernate Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program {
    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ru.itis.chat");
        EntityManager entityManager = factory.createEntityManager();

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        User user = User.builder()
                .username("Marsel")
                .build();
        entityManager.persist(user);
        transaction.commit();
    }
}
