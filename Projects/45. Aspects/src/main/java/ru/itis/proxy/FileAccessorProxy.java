package ru.itis.proxy;


/**
 * 08.07.2020
 * 45. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FileAccessorProxy extends FileAccessor {
    private FileAccessor target;

    private BeforeAdvice beforeAdvice;
    private AfterAdvice afterAdvice;
    private MethodAdvice methodAdvice;

    public FileAccessorProxy(FileAccessor target) {
        this.target = target;
    }

    @Override
    public void openFile(String fileName) {
        if (beforeAdvice != null) {
            beforeAdvice.before();
        }

        if (methodAdvice != null) {
            methodAdvice.run();
        } else {
            target.openFile(fileName);
        }

        if (afterAdvice != null) {
            afterAdvice.after();
        }
    }

    public void setBeforeAdvice(BeforeAdvice beforeAdvice) {
        this.beforeAdvice = beforeAdvice;
    }

    public void setAfterAdvice(AfterAdvice afterAdvice) {
        this.afterAdvice = afterAdvice;
    }

    public void setMethodAdvice(MethodAdvice methodAdvice) {
        this.methodAdvice = methodAdvice;
    }
}
