package ru.itis.proxy;

import java.util.Scanner;

/**
 * 08.07.2020
 * 45. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    public static void callOpenFileWithScanner(FileAccessor fileAccessor) {
        Scanner scanner = new Scanner(System.in);
        fileAccessor.openFile(scanner.nextLine());
    }

    public static void main(String[] args) {
        FileAccessor target = new FileAccessor();

        FileAccessorProxy proxy = new FileAccessorProxy(target);

        proxy.setBeforeAdvice(() -> System.out.println("Сейчас будет открыт файл"));
        proxy.setAfterAdvice(() -> System.out.println("Файл был открыт"));
        proxy.setMethodAdvice(() -> System.out.println("А это произошло вместо открытия файла"));
        callOpenFileWithScanner(proxy);
    }
}
