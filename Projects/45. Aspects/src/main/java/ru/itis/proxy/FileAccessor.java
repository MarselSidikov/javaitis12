package ru.itis.proxy;

import java.io.*;

/**
 * 08.07.2020
 * 45. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FileAccessor {
    public void openFile(String fileName) {
        try {
            InputStream inputStream = new FileInputStream(fileName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            System.out.println("Прочитано из файла: " + bufferedReader.readLine());
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
