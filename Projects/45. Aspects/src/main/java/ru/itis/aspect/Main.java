package ru.itis.aspect;

import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.aop.framework.ProxyFactory;

/**
 * 08.07.2020
 * 45. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        FileAccessor target = new FileAccessor();
        ProxyFactory proxyFactory = new ProxyFactory(target);

        MethodBeforeAdvice beforeAdvice = (method, arguments, targetObject) -> {
            System.out.println(method.getName());
            System.out.println("ОБъект был создан " + ((FileAccessor)targetObject).getCreatedObjectTime());
            System.out.println("Был открыт файл " + arguments[0]);
        };

        AfterReturningAdvice afterReturningAdvice = (returnValue, method, arguments, targetObject) -> {
            System.out.println(returnValue);
        };

        MethodInterceptor methodInterceptor = (invocation) -> {
            System.out.println("Мы запретили исходный вызов");
            return -100;
        };

        ThrowsAdvice throwsAdvice = new ThrowsAdviceImpl();

        proxyFactory.addAdvice(beforeAdvice);
        proxyFactory.addAdvice(afterReturningAdvice);
//        proxyFactory.addAdvice(methodInterceptor);
        proxyFactory.addAdvice(throwsAdvice);

        FileAccessor proxy = (FileAccessor) proxyFactory.getProxy();
        int result = proxy.openFile("input1.txt");
        System.out.println(result);

    }
}
