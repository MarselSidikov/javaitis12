package ru.itis.aspect;

import java.io.*;
import java.nio.file.attribute.FileAttribute;
import java.time.LocalDateTime;

/**
 * 08.07.2020
 * 45. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FileAccessor {

    private LocalDateTime createdObjectTime;

    public FileAccessor() {
        this.createdObjectTime = LocalDateTime.now();
    }

    public int openFile(String fileName) {
        try {
            InputStream inputStream = new FileInputStream(fileName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = bufferedReader.readLine();
            System.out.println("Прочитано из файла: " + line);
            return line.length();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public LocalDateTime getCreatedObjectTime() {
        return createdObjectTime;
    }
}
