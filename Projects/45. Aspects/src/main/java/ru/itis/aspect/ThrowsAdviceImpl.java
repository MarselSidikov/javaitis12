package ru.itis.aspect;

import org.springframework.aop.ThrowsAdvice;

/**
 * 08.07.2020
 * 45. Aspects
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ThrowsAdviceImpl implements ThrowsAdvice {
    public void afterThrowing(Exception ex) {
        System.out.println("Вы тут ошибочку словили " + ex.getMessage());
    }
}
