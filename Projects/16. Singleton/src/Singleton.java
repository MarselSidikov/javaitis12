public class Singleton {

    // глобальная объектная переменная, которая содержит
    // единственный экземпляр класса
    private final static Singleton instance;

    // при запуске JVM создается объект
    // кладется в поле instance
    static {
        instance = new Singleton();
    }

    private Singleton() {

    }
    // с помощью этого метода вы можете получить его экземпляр
    static Singleton getInstance() {
        return instance;
    }
}
