public class Logger {
    private static final Logger instance;

    static {
        instance = new Logger();
    }

    private Logger() {

    }

    static Logger getLogger() {
        return instance;
    }

    void info(String message) {
        System.out.println(message);
    }

    void error(String message) {
        System.err.println(message);
    }
}
