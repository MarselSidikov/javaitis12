package ru.itdrive.web.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.web.config.ApplicationConfig;
import ru.itdrive.web.models.User;
import ru.itdrive.web.services.UsersSearchService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 08.06.2020
 * 40. WebAppExample
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/users/search")
public class UsersSearchServlet extends HttpServlet {

    private UsersSearchService usersSearchService;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        usersSearchService = context.getBean(UsersSearchService.class);
    }

    // users/search?email=er
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String emailPattern = request.getParameter("email");
        List<User> users = usersSearchService.searchByEmail(emailPattern);
        String usersAsJson = objectMapper.writeValueAsString(users);

        PrintWriter writer = response.getWriter();
        response.setContentType("application/json");
        writer.println(usersAsJson);
    }
}
