package ru.itdrive.web.repositories;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.web.config.ApplicationConfig;
import ru.itdrive.web.models.User;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 09.06.2020
 * 40. AjaxSearchExample
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
class UsersRepositoryJdbcTemplateImplTest {

    UsersRepositoryJdbcTemplateImpl usersRepository;

    private static final User USER = User.builder()
            .id(4L)
            .email("USER")
            .confirmCode("0a4d8114-5b6c-4929-b72c-9f1a7d05293d")
            .password("USER")
            .build();

    @BeforeEach
    void setUp() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        usersRepository = context.getBean(UsersRepositoryJdbcTemplateImpl.class);
    }

    @AfterEach
    void after() {
        User userForUpdate = usersRepository.find(5L).get();
        userForUpdate.setEmail("TEMP");
        usersRepository.update(userForUpdate);
    }

    @Test
    void searchByEmail() {
        List<User> actual = usersRepository.searchByEmail("ER");
        User actualUser = actual.get(0);
        assertEquals(USER, actualUser);
    }

    @Test
    void findAll() {
        List<User> actual = usersRepository.findAll();
        assertEquals(actual.size(), 3);
    }

    @Test
    void findOnCorrectId() {
        Optional<User> actualOptional = usersRepository.find(4L);
        User actual = actualOptional.get();
        assertEquals(USER, actual);
    }

    @Test
    void findOnInCorrectId() {
        Optional<User> actualOptional = usersRepository.find(15L);
        assertFalse(actualOptional.isPresent());
    }

    @Test
    void update() {
        User userForUpdate = usersRepository.find(5L).get();
        userForUpdate.setEmail("NEW_EMAIL");
        usersRepository.update(userForUpdate);
        User userAfterUpdate = usersRepository.find(5L).get();
        assertEquals("NEW_EMAIL", userAfterUpdate.getEmail());
    }

    @Test
    void save() {
        User user = User.builder()
                .password("A")
                .email("A")
                .confirmCode("A")
                .build();
        usersRepository.save(user);
        System.out.println(user);
    }
}