package ru.itdrive.web.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itdrive.web.config.ApplicationConfig;
import ru.itdrive.web.models.User;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 09.06.2020
 * 40. AjaxSearchExample
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
class UsersRepositoryJdbcImplTest {

    UsersRepositoryJdbcImpl usersRepository;

    private static final User USER = User.builder()
            .id(4L)
            .email("USER")
            .confirmCode("0a4d8114-5b6c-4929-b72c-9f1a7d05293d")
            .password("USER")
            .build();

    @BeforeEach
    void setUp() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        usersRepository = context.getBean(UsersRepositoryJdbcImpl.class);
    }

    @Test
    void findOnCorrectId() {
        Optional<User> actualOptional = usersRepository.find(4L);
        User actual = actualOptional.get();
        assertEquals(USER, actual);
    }

    @Test
    void findOnInCorrectId() {
        Optional<User> actualOptional = usersRepository.find(15L);
        assertFalse(actualOptional.isPresent());
    }


    @Test
    void findAll() {
        List<User> actual = usersRepository.findAll();
        assertEquals(actual.size(), 3);
    }
}