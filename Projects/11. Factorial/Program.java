class Program {
	public static int fact(int n) {
		System.out.println("->> In fact(" + n + ")");
		if (n == 0) {
			System.out.println("<<- From fact(" + n + ") = " + 1);
			return 1;
		}
		int result = fact(n - 1) * n;
		System.out.println("<<- From fact(" + n + ") = " + result);
		return result;
	}

	public static void main(String args[]) {
		System.out.println(fact(5));
	}
}