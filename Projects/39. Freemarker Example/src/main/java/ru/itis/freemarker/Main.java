package ru.itis.freemarker;

import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.File;
import java.io.FileWriter;
import java.util.*;

/**
 * 04.06.2020
 * 39. Freemarker Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_0);

        configuration.setDefaultEncoding("UTF-8");
//        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.DEBUG_HANDLER);
        configuration.setTemplateLoader(new FileTemplateLoader(new File("C:\\Users\\Marsel\\Desktop\\Education\\Current\\" +
                "javaitis12\\Projects\\39. Fremarker Example\\src\\main\\resources")));

        Template template = configuration.getTemplate("template_for_web.ftlh");

        User user = User.builder()
                .age(26)
                .name("Marsel")
                .id(10L)
                .build();

        List<User> users = new ArrayList<>();

        users.add(User.builder().age(19).name("Igor").build());
        users.add(User.builder().age(24).name("Vladimir").build());
        users.add(User.builder().age(33).name("Sergey").build());
        users.add(User.builder().age(48).name("Semen").build());

        Map<String, Object> attributes = new HashMap<>();
        attributes.put("user", user);
        attributes.put("users", users);

        FileWriter writer = new FileWriter("output.html");
        template.process(attributes, writer);
    }
}
