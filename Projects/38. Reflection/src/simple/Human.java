package simple;

/**
 * 03.06.2020
 * 38. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human {
    private int age;
    double height;

    private Human() {

    }

    public void printAge() {
        System.out.println(age);
    }

    public void grow(double value) {
        this.height += value;
    }

    public void grow() {
        this.height += 0.01;
    }

    public double getHeight() {
        return height;
    }

    private void simplePrivate() {
        System.out.println("I'm private");
    }
}
