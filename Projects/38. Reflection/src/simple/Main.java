package simple;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

// Получать информацию о классе по его имени

// Получать информацию обо всех полях класса (приватные/публичные, их названия, их типы)

// Получить информацию обо всех методах класса (приватные/публичные, их названия, типы возвращаемого значения,

// типы формальных параметров)

// Изменять значения полей
// Вызывать методы над объектами
// Создавать объекты используя конструктор
public class Main {

    public static void main(String[] args) throws Exception {
//        simple.Human human = new simple.Human();
//        human.age = 26;
//        human.height = 1.85;
//        human.grow();
//        human.grow();
//        System.out.println(human.height);
//        human.printAge();

        Class<?> aClass = Class.forName("simple.Human"); // загружает класс в виртуальную машину,
        // я еще и получил описание этого класса -> Class
        System.out.println(aClass.getName());

        Field fields[] = aClass.getDeclaredFields(); // возвращает массив полей класса
        for (Field field : fields) {
            System.out.print(field.getName() + " ");
            System.out.println(field.getType());
        }

//        simple.Human human = (simple.Human)aClass.newInstance();
        Constructor constructor = aClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        Human human = (Human) constructor.newInstance();

        human.height = 1.85;

        Field field = aClass.getDeclaredField("age");
        field.setAccessible(true);
        field.set(human, 33);
        human.printAge();

        Method methods[] = aClass.getDeclaredMethods();

        for (Method method : methods) {
            System.out.print(method.getName() + " ");
            System.out.print(method.getReturnType() + " ");
            System.out.println(method.getParameterCount() + " ");
        }

//        Method method = aClass.getDeclaredMethod("grow");
        Method method = aClass.getMethod("grow", Double.TYPE);
        method.invoke(human, 0.02);
        System.out.println(human.height);

        Method methodWithReturn = aClass.getMethod("getHeight");
        Object result = methodWithReturn.invoke(human);
        System.out.println(result);
    }
}
