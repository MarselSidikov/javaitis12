package framework;

import java.io.OutputStream;
import java.util.Random;

/**
 * 03.06.2020
 * 38. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Terminal {
    private Printer redPrinter;
    private Printer simplePrinter;

    public void giveMoney() {
        Random random = new Random();
        redPrinter.print("Выдана сумма - " + random.nextInt(100));
        simplePrinter.print("Выдана сумма - " + random.nextInt(100));
    }
}
