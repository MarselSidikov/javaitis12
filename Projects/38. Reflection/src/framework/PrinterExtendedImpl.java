package framework;

import java.time.LocalDateTime;

/**
 * 03.06.2020
 * 38. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PrinterExtendedImpl implements Printer {
    @Override
    public void print(String message) {
        System.err.println(LocalDateTime.now() + " " + message);
    }
}
