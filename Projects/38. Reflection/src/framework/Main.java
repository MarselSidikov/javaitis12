package framework;


/**
 * 03.06.2020
 * 38. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Container container = new Container();
        container.initialize();
        Terminal terminal = (Terminal) container.getComponent("terminal");
        terminal.giveMoney();
    }
}
