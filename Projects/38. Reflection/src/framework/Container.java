package framework;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

/**
 * 03.06.2020
 * 38. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Container {
    private Map<String, Object> components;
    private Map<String, String> propertyValues;

    public void initialize() {
        propertyValues = new HashMap<>();
        components = new HashMap<>();
        loadProperties();
        loadComponents();
        injecting();
    }

    private void injecting() {
        for (Map.Entry<String, String> property : propertyValues.entrySet()) {
            if (property.getKey().startsWith("dependency")) {
                String splitted[] = property.getKey().split("\\.");
                String firstComponentName = splitted[1];
                String secondComponentName = splitted[2];
                Object firstComponent = components.get(firstComponentName);
                Object secondComponent = components.get(secondComponentName);

                try {
                    Field field = firstComponent.getClass().getDeclaredField(secondComponentName);
                    field.setAccessible(true);
                    field.set(firstComponent, secondComponent);
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    throw new IllegalArgumentException();
                }
            }
        }
    }

    private void loadComponents() {
        for (Map.Entry<String, String> property : propertyValues.entrySet()) {
            if (property.getKey().contains(".impl")) {
                int nameEndPosition = property.getKey().lastIndexOf(".impl");
                String componentName = property.getKey().substring(0, nameEndPosition);
                try {
                    Class componentClass = Class.forName(property.getValue());
                    Object componentInstance = componentClass.newInstance();
                    components.put(componentName, componentInstance);
                } catch (ClassNotFoundException | InstantiationException e) {
                    throw new IllegalArgumentException(e);
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException();
                }
            }
        }
    }

    private void loadProperties() {
        Properties properties = new Properties();
        try {
            properties.load(new FileReader("context.properties"));
            Set<String> propertyNames = properties.stringPropertyNames();
            for (String property : propertyNames) {
                propertyValues.put(property, properties.getProperty(property));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Object getComponent(String componentName) {
        return components.get(componentName);
    }
}
