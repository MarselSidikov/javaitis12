import java.util.Scanner;

class Program {

    public static boolean isUpperEnglishLetter(char c) {
        return c >= 'A' && c <= 'Z';
    }

    public static boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    public static void main(String[] args) {
        char c = 'А';
        int codeOfA = c;

        int codeOfБ = codeOfA + 1;
        char c2 = (char) codeOfБ;
        System.out.println(codeOfA);
        System.out.println(c2);

        char message[] = {'П', 'р', 'и', 'в', 'e', 'т'};
        char message2[] = "Привет".toCharArray();

		Scanner scanner = new Scanner(System.in);
		char message3[] = scanner.nextLine().toCharArray();

		for (int i = 0; i < message3.length; i++) {
			System.out.println("Is Digit " + message3[i] + "? - " + isDigit(message3[i]));
		}
    }
}