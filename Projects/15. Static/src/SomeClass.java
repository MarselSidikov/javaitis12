import java.util.Random;

public class SomeClass {
    // у каждого объекта свой field
    int field;
    // у каждого объекта один и тот же staticField
    static int staticField = 10;

    // статический инициализатор
    static {
        Random random = new Random();
        staticField = staticField * random.nextInt(10);
    }

    void method() {
        System.out.println("From method: field = " + field);
        System.out.println("From method: staticField = " + staticField);
    }

    static void staticMethod() {
        // System.out.println("From method: field = " + field);
        System.out.println("From method: staticField = " + staticField);
    }
}
