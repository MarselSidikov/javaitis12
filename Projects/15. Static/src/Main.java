public class Main {

    public static void main(String[] args) {

        System.out.println(SomeClass.staticField);

        SomeClass.staticField = 50;

        SomeClass a = new SomeClass();
        a.field = 100;
        System.out.println(a.staticField);
        a.staticField = 300;

        SomeClass b = new SomeClass();
        b.field = 200;
        b.staticField = 400;

        System.out.println(a.field);
        System.out.println(b.field);
        System.out.println(a.staticField);
        System.out.println(b.staticField);

        a.method();

        SomeClass.staticMethod();
    }
}
