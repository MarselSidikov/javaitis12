package ru.itis.publishers;

import reactor.core.publisher.Flux;
import ru.itis.dto.FileInfo;

/**
 * 19.02.2021
 * Reactive
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FileInfoService {
    Flux<FileInfo> getFileLength(Flux<String> fileUrls);
    Flux<FileInfo> getFileMime(Flux<String> fileUrls);
    Flux<FileInfo> getCompletedFileInfo(Flux<FileInfo> filesWithLength, Flux<FileInfo> fileWithMime);
}
