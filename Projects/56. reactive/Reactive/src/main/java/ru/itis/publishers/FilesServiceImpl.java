package ru.itis.publishers;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Callable;
import java.util.stream.Stream;

/**
 * 11.02.2021
 * Reactive
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FilesServiceImpl implements FilesService {

    @Override
    public Flux<String> getFileUrls() {
        try {
            return Flux.using(() -> Files.lines(Paths.get("images.txt")),
                    Flux::fromStream,
                    Stream::close);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
}
