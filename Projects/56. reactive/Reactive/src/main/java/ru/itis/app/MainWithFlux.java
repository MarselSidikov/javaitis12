package ru.itis.app;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import ru.itis.dto.FileInfo;
import ru.itis.publishers.FileInfoService;
import ru.itis.publishers.FileInfoServiceImpl;
import ru.itis.publishers.FilesService;
import ru.itis.publishers.FilesServiceImpl;
import ru.itis.utils.FileUtils;

import java.util.Scanner;

/**
 * 11.02.2021
 * Reactive
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainWithFlux {
    public static void main(String[] args) throws Exception {

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        FilesService filesService = new FilesServiceImpl();
        FileInfoService fileInfoService = new FileInfoServiceImpl();

        Flux<String> fileUrls = filesService.getFileUrls();

        Flux<FileInfo> filesWithLength = fileInfoService.getFileLength(fileUrls);
        Flux<FileInfo> filesWithMime = fileInfoService.getFileMime(fileUrls);
        Flux<FileInfo> completedFiles = fileInfoService.getCompletedFileInfo(filesWithLength, filesWithMime);

        completedFiles.subscribe(System.out::println);

        Thread.sleep(40000);
    }
}
