package ru.itis.publishers;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import ru.itis.dto.FileInfo;
import ru.itis.utils.FileUtils;

/**
 * 19.02.2021
 * Reactive
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FileInfoServiceImpl implements FileInfoService {

    private final FileUtils fileUtils;
    private final Scheduler fileMimeScheduler;
    private final Scheduler fileLengthScheduler;

    public FileInfoServiceImpl() {
        this.fileUtils = new FileUtils();
        this.fileMimeScheduler = Schedulers.newParallel("MIME Scheduler");
        this.fileLengthScheduler = Schedulers.newParallel("LENGTH Scheduler");
    }

    @Override
    public Flux<FileInfo> getFileLength(Flux<String> fileUrls) {
        return Flux.create(emitter -> fileUrls.publishOn(fileMimeScheduler).subscribe(fileUrl ->
                emitter.next(fileUtils.getSize(fileUrl))));
    }

    @Override
    public Flux<FileInfo> getFileMime(Flux<String> fileUrls) {
        return Flux.create(emitter -> fileUrls.publishOn(fileLengthScheduler).subscribe(fileUrl ->
                emitter.next(fileUtils.getMime(fileUrl))));
    }

    @Override
    public Flux<FileInfo> getCompletedFileInfo(Flux<FileInfo> filesWithLength, Flux<FileInfo> filesWithMime) {
        return Flux.zip(filesWithLength, filesWithMime, (fileWithLength, fileWithMime) -> {
            if (fileWithLength.getUrl().equals(fileWithMime.getUrl()) &&
                    fileWithLength.isAvailable() && fileWithMime.isAvailable()) {
                return FileInfo.builder()
                        .MIME(fileWithMime.getMIME())
                        .url(fileWithLength.getUrl())
                        .size(fileWithLength.getSize())
                        .isAvailable(true)
                        .build();
            }
            return FileInfo.builder()
                    .isAvailable(false)
                    .build();
        }).filter(FileInfo::isAvailable);
    }
}
