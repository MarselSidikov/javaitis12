package ru.itis.utils;

import lombok.SneakyThrows;
import ru.itis.dto.FileInfo;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * 18.02.2021
 * Reactive
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FileUtils {
    public FileInfo getSize(String fileUrl) {
        FileInfo fileInfo = FileInfo.builder()
                .url(fileUrl)
                .isAvailable(true)
                .build();

        URL url;
        try {
            url = new URL(fileUrl);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
        // create a connection
        HttpURLConnection connection;
        try {
            // open stream to get size of page
            connection = (HttpURLConnection) url.openConnection();
            // set request method.
            connection.setRequestMethod("HEAD");
            // get the input stream of process
            connection.getInputStream();
            // store size of file
            BigInteger size = BigInteger.valueOf(connection.getContentLength());
            // print the size of downloaded file
            connection.getInputStream().close();
            fileInfo.setSize(size.longValue());

            if (fileInfo.getSize() == -1) {
                fileInfo.setAvailable(false);
            }
        } catch (Exception e) {
            fileInfo.setAvailable(false);
        }
        return fileInfo;
    }

    @SneakyThrows
    public FileInfo getMime(String fileUrl) {
        FileInfo fileInfo = FileInfo
                .builder()
                .isAvailable(true)
                .url(fileUrl)
                .build();
        try {
            URL url = new URL(fileUrl);
            URLConnection urlConnection;
            urlConnection = url.openConnection();
            fileInfo.setMIME(urlConnection.getContentType());
        } catch (Exception e) {
            fileInfo.setAvailable(false);
        }
        return fileInfo;
    }
}
