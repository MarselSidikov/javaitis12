package ru.itis.publishers;

import reactor.core.publisher.Flux;
import ru.itis.dto.FileInfo;

import java.util.List;

/**
 * 11.02.2021
 * Reactive
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FilesService {
    Flux<String> getFileUrls();
}
