package ru.itis.files.mime.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import ru.itis.files.mime.dto.FileInfo;
import ru.itis.files.mime.services.FilesMimeService;

/**
 * 19.02.2021
 * files-mime-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
public class FilesMimeController {

    @Autowired
    private FilesMimeService filesMimeService;

    @GetMapping(value = "files/mime", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<FileInfo> getFilesWithMime() {
        return filesMimeService.getFileMime();
    }
}
