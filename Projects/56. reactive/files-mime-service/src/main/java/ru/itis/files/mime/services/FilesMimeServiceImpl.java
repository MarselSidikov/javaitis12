package ru.itis.files.mime.services;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import ru.itis.files.mime.dto.FileInfo;

import java.net.URL;
import java.net.URLConnection;

/**
 * 19.02.2021
 * files-mime-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component
public class FilesMimeServiceImpl implements FilesMimeService {

    private WebClient webClient = WebClient.create();

    @Override
    public Flux<FileInfo> getFileMime() {
        Flux<FileInfo> fileUrls = webClient.get()
                .uri("http://localhost:8080/files")
                .retrieve()
                .bodyToFlux(FileInfo.class);

        return Flux.<FileInfo>create(emitter -> fileUrls.publishOn(Schedulers.boundedElastic()).subscribe(fileUrl ->
                emitter.next(getMime(fileUrl)))).filter(FileInfo::isAvailable);
    }

    private FileInfo getMime(FileInfo fileInfoWithUrl) {
        FileInfo fileInfo = FileInfo
                .builder()
                .isAvailable(true)
                .url(fileInfoWithUrl.getUrl())
                .build();
        try {
            URL url = new URL(fileInfoWithUrl.getUrl());
            URLConnection urlConnection;
            urlConnection = url.openConnection();
            fileInfo.setMIME(urlConnection.getContentType());
        } catch (Exception e) {
            fileInfo.setAvailable(false);
        }
        return fileInfo;
    }
}
