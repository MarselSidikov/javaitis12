package ru.itis.files.mime.services;

import reactor.core.publisher.Flux;
import ru.itis.files.mime.dto.FileInfo;

/**
 * 19.02.2021
 * files-mime-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FilesMimeService {
    Flux<FileInfo> getFileMime();
}
