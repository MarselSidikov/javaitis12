package ru.itis.files.services;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import ru.itis.files.dto.FileInfo;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * 11.02.2021
 * Reactive
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class FilesServiceImpl implements FilesService {

    @Override
    public Flux<FileInfo> getFileUrls() {
        try {
            return Flux.using(() -> Files.lines(Paths.get("images.txt")),
                    Flux::fromStream,
                    Stream::close).map(fileUrl -> FileInfo.builder()
                        .url(fileUrl)
                        .build());
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
}
