package ru.itis.files.services;

import reactor.core.publisher.Flux;
import ru.itis.files.dto.FileInfo;

/**
 * 11.02.2021
 * Reactive
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FilesService {
    Flux<FileInfo> getFileUrls();
}
