package ru.itis.files.size.services;

import reactor.core.publisher.Flux;
import ru.itis.files.size.dto.FileInfo;

/**
 * 19.02.2021
 * files-mime-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FilesSizeService {
    Flux<FileInfo> getFileSize();
}
