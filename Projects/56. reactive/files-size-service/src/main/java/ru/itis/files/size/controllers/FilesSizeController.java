package ru.itis.files.size.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import ru.itis.files.size.dto.FileInfo;
import ru.itis.files.size.services.FilesSizeService;

/**
 * 19.02.2021
 * files-mime-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
public class FilesSizeController {

    @Autowired
    private FilesSizeService filesSizeService;

    @GetMapping(value = "files/size", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<FileInfo> getFilesWithSize() {
        return filesSizeService.getFileSize();
    }
}
