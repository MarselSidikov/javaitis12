package ru.itis.files.size.services;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import ru.itis.files.size.dto.FileInfo;

import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * 19.02.2021
 * files-size-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component
public class FilesSizeServiceImpl implements FilesSizeService {

    private WebClient webClient = WebClient.create();

    @Override
    public Flux<FileInfo> getFileSize() {
        Flux<FileInfo> fileUrls = webClient.get()
                .uri("http://localhost:8080/files")
                .retrieve()
                .bodyToFlux(FileInfo.class);

        return Flux.<FileInfo>create(emitter -> fileUrls.publishOn(Schedulers.boundedElastic()).subscribe(fileUrl ->
                emitter.next(getSize(fileUrl)))).filter(FileInfo::isAvailable);
    }

    private FileInfo getSize(FileInfo fileInfoWithUrl) {
        FileInfo fileInfo = FileInfo.builder()
                .url(fileInfoWithUrl.getUrl())
                .isAvailable(true)
                .build();

        URL url;
        try {
            url = new URL(fileInfoWithUrl.getUrl());
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
        // create a connection
        HttpURLConnection connection;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("HEAD");
            connection.getInputStream();
            BigInteger size = BigInteger.valueOf(connection.getContentLength());
            connection.getInputStream().close();
            fileInfo.setSize(size.longValue());

            if (fileInfo.getSize() == -1) {
                fileInfo.setAvailable(false);
            }
        } catch (Exception e) {
            fileInfo.setAvailable(false);
        }
        return fileInfo;
    }
}
