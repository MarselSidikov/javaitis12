package ru.itis.files.size;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilesSizeServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilesSizeServiceApplication.class, args);
    }

}
