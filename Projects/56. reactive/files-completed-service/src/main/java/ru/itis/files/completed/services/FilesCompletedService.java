package ru.itis.files.completed.services;

import reactor.core.publisher.Flux;
import ru.itis.files.completed.dto.FileInfo;

/**
 * 19.02.2021
 * files-mime-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FilesCompletedService {
    Flux<FileInfo> getFileComplete();
}
