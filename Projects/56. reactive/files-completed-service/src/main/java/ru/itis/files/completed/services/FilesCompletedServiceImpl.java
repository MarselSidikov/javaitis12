package ru.itis.files.completed.services;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import ru.itis.files.completed.dto.FileInfo;

import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 19.02.2021
 * files-size-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component
public class FilesCompletedServiceImpl implements FilesCompletedService {

    private WebClient webClient = WebClient.create();

    @Override
    public Flux<FileInfo> getFileComplete() {
        Flux<FileInfo> filesWithMimes = webClient.get()
                .uri("http://localhost:8081/files/mime")
                .retrieve()
                .bodyToFlux(FileInfo.class).publishOn(Schedulers.boundedElastic());

        Flux<FileInfo> filesWithSize = webClient.get()
                .uri("http://localhost:8082/files/size")
                .retrieve()
                .bodyToFlux(FileInfo.class).publishOn(Schedulers.boundedElastic());

        return Flux.zip(filesWithSize, filesWithMimes, (fileWithLength, fileWithMime) -> {
            if (fileWithLength.getUrl().equals(fileWithMime.getUrl()) &&
                    fileWithLength.isAvailable() && fileWithMime.isAvailable()) {
                return FileInfo.builder()
                        .MIME(fileWithMime.getMIME())
                        .url(fileWithLength.getUrl())
                        .size(fileWithLength.getSize())
                        .isAvailable(true)
                        .build();
            }

            return FileInfo.builder()
                    .isAvailable(false)
                    .build();
        }).filter(FileInfo::isAvailable);
    }
}
