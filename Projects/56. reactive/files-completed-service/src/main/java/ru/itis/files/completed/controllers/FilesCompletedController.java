package ru.itis.files.completed.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import ru.itis.files.completed.dto.FileInfo;
import ru.itis.files.completed.services.FilesCompletedService;

/**
 * 19.02.2021
 * files-mime-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
public class FilesCompletedController {

    @Autowired
    private FilesCompletedService filesCompletedService;

    @CrossOrigin
    @GetMapping(value = "files/completed", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<FileInfo> getFilesCompleted() {
        return filesCompletedService.getFileComplete();
    }
}
