package ru.itis.files.completed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilesCompletedServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilesCompletedServiceApplication.class, args);
    }

}
