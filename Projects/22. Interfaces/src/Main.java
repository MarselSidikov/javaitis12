public class Main {

    public static void main(String[] args) {
        GPS gps = new GPS();
        Phone phone = new Phone();
        PhotoCamera photoCamera = new PhotoCamera();
        Smartphone smartphone = new Smartphone();

        Caller callers[] = {phone, smartphone};
        LocationResolver locationResolvers[] = {smartphone, gps};
        PhotoCreator photoCreators[] = {smartphone, photoCamera};

        callers[0].call("Марсель");
        callers[1].call("Марсель");

        locationResolvers[0].getLocation();
        locationResolvers[1].getLocation();

        photoCreators[0].createPhoto();
        photoCreators[1].createPhoto();

    }
}
