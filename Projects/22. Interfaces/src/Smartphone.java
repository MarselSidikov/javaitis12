public class Smartphone implements LocationResolver, PhotoCreator, Caller {

    public void createPhoto() {
        System.out.println("Смартфон делает фото");
    }

    public void call(String number) {
        System.out.println("Смартфон звонит на " + number);
    }

    public void getLocation() {
        System.out.println("Смартфон не знает где вы находитесь");
    }
}
