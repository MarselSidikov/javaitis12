package ru.itis;

public class HasCodesForBaseTypes {
    public static void main(String[] args) {
        Boolean b1 = true;
        Boolean b2 = false;
        Character c1 = 'A';
        Character c2 = 'B';
        Double d1 = 1.0;
        Double d2 = 2.0;
        Short s1 = 50;
        Short s2 = 65;

        System.out.println(b1.hashCode());
        System.out.println(b2.hashCode());
        System.out.println(c1.hashCode());
        System.out.println(c2.hashCode());
        System.out.println(d1.hashCode());
        System.out.println(d2.hashCode());
        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
    }
}
