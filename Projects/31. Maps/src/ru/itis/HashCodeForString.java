package ru.itis;

import java.util.Arrays;

public class HashCodeForString {
    public static void main(String[] args) {
        String text = "Marsel";
        char chars[] = text.toCharArray();
        int codes[] = new int[chars.length];
        for (int i = 0; i < chars.length; i++) {
            codes[i] = chars[i];
        }
        System.out.println(Arrays.toString(codes));
        int hash = 0;
        for (int i = 0; i < chars.length; i++) {
            hash += chars[i] * i;
        }
        System.out.println(hash);
    }
}
