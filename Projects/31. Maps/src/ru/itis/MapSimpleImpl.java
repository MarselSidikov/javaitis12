package ru.itis;

public class MapSimpleImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 10;
    MapEntry<K, V> entries[];
    private int count;

    public MapSimpleImpl() {
        entries = new MapEntry[DEFAULT_SIZE];
        count = 0;
    }

    @Override
    public V get(K key) {
        for (int i = 0; i < count; i++) {
            MapEntry<K, V> entry = entries[i];
            if (entry.key.equals(key)) {
                return entry.value;
            }
        }
        return null;
    }

    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        this.entries[count] = newMapEntry;
        count++;
    }

    static class MapEntry<K, V> {
        K key;
        V value;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}
