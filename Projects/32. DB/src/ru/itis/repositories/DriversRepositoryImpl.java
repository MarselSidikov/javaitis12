package ru.itis.repositories;

import ru.itis.models.Driver;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DriversRepositoryImpl implements DriversRepository {

    private Connection connection;

    //language=SQL
    private static final String SQL_INSERT_DRIVER =
            "insert into driver(first_name, last_name, age) values (?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_DRIVER_BY_ID =
            "select * from driver where id = ?";

    //language=SQL
    private static final String SQL_UPDATE_DRIVER_BY_ID =
            "update driver set first_name = ?, last_name = ?, age = ? where id = ?";

    //language=SQL
    private static final String SQL_SELECT_ALL_DRIVERS =
            "select * from driver";

    public DriversRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void save(Driver entity) {
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        try {
//            Statement statement = connection.createStatement();
//            // TODO: insecure-код
//            //language=SQL
//            String sql = "insert into driver(first_name, last_name, age) values ('" +
//                    entity.getFirstName() + "','" + entity.getLastName() + "', " + entity.getAge() + ");";
//            System.out.println(sql);
//            statement.executeUpdate(sql);
//            statement.close();
            statement = connection.prepareStatement(SQL_INSERT_DRIVER, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setInt(3, entity.getAge());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Driver not saved");
            }

            generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                int generatedId = generatedKeys.getInt("id");
                entity.setId(generatedId);
            } else {
                throw new SQLException("Exception in save for driver");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (generatedKeys != null) {
                    generatedKeys.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Optional<Driver> findBy(Integer id) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(SQL_SELECT_DRIVER_BY_ID);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            Driver driver = null;
            if (resultSet.next()) {
                driver = new Driver(resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getInt("age"));
            }
            return Optional.ofNullable(driver);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void update(Driver entity) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_UPDATE_DRIVER_BY_ID);
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setInt(3, entity.getAge());
            statement.setInt(4, entity.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Driver not updated");
            }
            statement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void deleteBy(Integer id) {

    }

    @Override
    public List<Driver> findAll() {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Driver> drivers = null;
        try {
            statement = connection.prepareStatement(SQL_SELECT_ALL_DRIVERS);
            resultSet = statement.executeQuery();
            drivers = new ArrayList<>();
            while (resultSet.next()) {
                Driver driver = new Driver(resultSet.getInt("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getInt("age"));
                drivers.add(driver);
            }
            return drivers;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
