package ru.itis.repositories;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T, ID> {
    void save(T entity);
    Optional<T> findBy(ID id);
    void update(T entity);
    void deleteBy(ID id);

    List<T> findAll();
}
