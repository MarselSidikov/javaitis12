package ru.itis.repositories;

import ru.itis.models.Driver;

public interface DriversRepository extends CrudRepository<Driver, Integer> {
}
