package ru.itis;

import ru.itis.models.Driver;
import ru.itis.repositories.DriversRepository;
import ru.itis.repositories.DriversRepositoryImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Optional;

public class MainForUpdate {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/java_itis_12";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "qwerty007";

    public static void main(String[] args) throws Exception {
        Connection connection =
                DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        DriversRepository driversRepository = new DriversRepositoryImpl(connection);

        Optional<Driver> driverOptional = driversRepository.findBy(77);
        Driver driver = driverOptional.orElse(new Driver(1, "Водитель", "Водитлов", 30));

        driver.setAge(40);
        driversRepository.update(driver);

    }
}
