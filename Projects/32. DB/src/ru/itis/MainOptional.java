package ru.itis;

import ru.itis.models.Driver;

import java.util.Optional;

public class MainOptional {
    public static void main(String[] args) {
//        Driver driver = new Driver(1, "Марсель", "Сидиков", 26);
//        Optional<Driver> driverOptional = Optional.of(driver);

        Driver driver = null;
        Optional<Driver> driverOptional = Optional.ofNullable(driver);

        if (driverOptional.isPresent()) {
            System.out.println(driverOptional.get());
        } else {
            System.out.println("Нет водителя!");
        }
    }
}
