package ru.itis;

import ru.itis.models.Driver;
import ru.itis.repositories.DriversRepository;
import ru.itis.repositories.DriversRepositoryImpl;

import java.sql.*;
import java.util.Scanner;

public class Main {
    //language=SQL
    private static final String SQL_SELECT_ALL_DRIVERS = "select * from driver";

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/java_itis_12";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "qwerty007";

    public static void main(String[] args) {
        try {
            Connection connection =
                    DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

//            Statement statement = connection.createStatement();
//            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_DRIVERS);
//            while (resultSet.next()) {
//                System.out.print(resultSet.getInt("id") + " ");
//                System.out.print(resultSet.getString("first_name") + " ");
//                System.out.print(resultSet.getString("last_name") + " ");
//                System.out.print(resultSet.getString("age"));
//                System.out.println();
//            }
//            resultSet.close();
//            statement.close();
//            connection.close();
            DriversRepository driversRepository = new DriversRepositoryImpl(connection);
            Scanner scanner = new Scanner(System.in);
            Driver driver = new Driver(
                    scanner.nextLine(),
                    scanner.nextLine(),
                    scanner.nextInt()
            );

            driversRepository.save(driver);
            connection.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
