package ru.itis;

import ru.itis.models.Driver;
import ru.itis.repositories.DriversRepository;
import ru.itis.repositories.DriversRepositoryImpl;

import java.sql.Connection;
import java.sql.DriverManager;

public class MainForSave {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/java_itis_12";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "qwerty007";

    public static void main(String[] args) throws Exception {
        Connection connection =
                DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        DriversRepository driversRepository = new DriversRepositoryImpl(connection);
        Driver driver = new Driver("Евгений", "Жуков", 45);
        driversRepository.save(driver);

        driver.setFirstName("Алексей");
        driversRepository.update(driver);

    }
}
