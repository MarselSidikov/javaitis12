class Program {
	public static void main(String[] args) {
		/**
		boolean e = true;
		e = false;

		int x = 16;

		e = x % 2 == 0;

		if (e == true) {
			System.out.println("Even");
		} else {
			System.out.println("Odd");
		}
		**/

		/**
		int x = 16;

		boolean e = x % 2 == 0;

		if (e) {
			System.out.println("Even");
		} else {
			System.out.println("Odd");
		}
		**/

		int x = 16;

		if (x % 2 == 0) {
			System.out.println("Even");
		} else {
			System.out.println("Odd");
		}


	}
}