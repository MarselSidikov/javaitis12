package ru.itis;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * 12.07.2020
 * 46. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) throws Exception {
        File file = new File("words.txt");
        InputStream in = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        Scanner scanner = new Scanner(System.in);
        scanner.nextInt();

        reader.lines().parallel()
                .map(string -> new Word(string, string.length()))
                .filter(word -> word.getFirstLetter() % 2 == 0)
                .sorted(Comparator.comparingInt(Word::getLength))
                .forEach(System.out::println);
    }
}