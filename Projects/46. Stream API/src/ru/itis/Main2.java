package ru.itis;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/**
 * 12.07.2020
 * 46. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) throws Exception {
        File file = new File("words.txt");
        InputStream in = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line = reader.readLine();
        List<String> stringsList = new ArrayList<>();

        while (line != null) {
            stringsList.add(line);
            line = reader.readLine();
        }

        Stream<String> stringsStream = stringsList.stream();

        Stream<Word> wordsStream = stringsStream.map(string -> new Word(string, string.length()));
        Stream<Word> filteredWords = wordsStream.filter(word -> word.getFirstLetter() == 'F');
//        Stream<Word> orderedWords = wordsStream.sorted( (firstWord, secondWord) -> {
//            return firstWord.getLength() - secondWord.getLength();
//        });
        Stream<Word> orderedWords = filteredWords.sorted(Comparator.comparingInt(Word::getLength));

//        orderedWords.forEach(word -> System.out.println(word));

        orderedWords.forEach(System.out::println);
    }
}
