package ru.itis;

import java.util.StringJoiner;

/**
 * 12.07.2020
 * 46. Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Word {
    private String text;
    private Integer length;
    private char firstLetter;

    public Word(String text, Integer length) {
        this.text = text;
        this.length = length;
        this.firstLetter = text.charAt(0);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Word.class.getSimpleName() + "[", "]")
                .add("text='" + text + "'")
                .add("length=" + length)
                .toString();
    }

    public char getFirstLetter() {
        return firstLetter;
    }
}
