package ru.itis;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main1 {

    public static void main(String[] args) throws Exception {
	    File file = new File("words.txt");
	    InputStream in = new FileInputStream(file);
	    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String word = reader.readLine();
		List<String> stringsList = new ArrayList<>();

		while (word != null) {
			stringsList.add(word);
			word = reader.readLine();
		}

	    Stream<String> stringsStream = stringsList.stream();

		Function<String, Integer> lengthFunction = string -> {
			return string.length();
		};

		Stream<Integer> integersStream = stringsStream.map(lengthFunction);

		Predicate<Integer> evensPredicate = integer -> {
			return integer % 2 == 0;
		};

		Stream<Integer> evensIntegersStream = integersStream.filter(evensPredicate);

		Consumer<Integer> printConsumer = integer -> {
			System.out.println(integer);
		};

		evensIntegersStream.forEach(printConsumer);
    }
}
