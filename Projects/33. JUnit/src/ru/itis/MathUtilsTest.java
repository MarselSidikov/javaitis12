package ru.itis;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MathUtilsTest {

    // тестируем этот объект
    private MathUtils mathUtils;

    // данный метод будет вызываться перед каждым тестом
    // здесь мы все время создаем объект заново
    // чтобы тесты проходили изолированно друг от друга
    @Before
    public void setUp() {
        this.mathUtils = new MathUtils();
    }

    @Test
    public void testIsPrimeOn2() {
//        1 и 2 boolean expected = true;
        boolean actual = mathUtils.isPrime(2);
//        1. Assert.assertEquals(expected, actual);
//        2. assertEquals(expected, actual);
        assertTrue(actual);
    }

    @Test
    public void testIsPrimeOn113() {
        assertTrue(mathUtils.isPrime(113));
    }

    @Test
    public void testIsPrimeOn72() {
        assertFalse(mathUtils.isPrime(72));
    }

    @Test
    public void testIsPrimeOn169() {
        assertFalse(mathUtils.isPrime(169));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsPrimeOn0() {
        mathUtils.isPrime(0);
    }
}
