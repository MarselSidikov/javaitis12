package ru.itis;

import java.time.LocalTime;
import java.util.*;

public class Main {

    public static void main(String[] args) {
	    MathUtils mathUtils = new MathUtils();

        System.out.println(mathUtils.isPrime(2));
        System.out.println(mathUtils.isPrime(3));
        System.out.println(mathUtils.isPrime(113));
        System.out.println(mathUtils.isPrime(72));
        System.out.println(mathUtils.isPrime(169));
        System.out.println(mathUtils.isPrime(0));
    }
}
