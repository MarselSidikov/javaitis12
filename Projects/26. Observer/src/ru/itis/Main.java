package ru.itis;

public class Main {

    public static void main(String[] args) {
	    Button button = new Button();
	    Printer printer = new Printer();
	    RedPrinter redPrinter = new RedPrinter();
	    button.addObserver(redPrinter);
	    button.addObserver(printer);
	    button.click();
    }
}
