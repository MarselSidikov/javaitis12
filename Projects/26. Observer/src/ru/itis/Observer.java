package ru.itis;

public interface Observer {
    void handle();
}
