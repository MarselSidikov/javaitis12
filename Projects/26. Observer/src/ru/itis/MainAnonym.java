package ru.itis;

public class MainAnonym {
    public static void main(String[] args) {
        Observable observable = new Observable() {
            Observer observer;
            @Override
            public void addObserver(Observer observer) {
                System.out.println("Добавили наблюдателя");
                this.observer = observer;
            }

            @Override
            public void notifyObservers() {
                System.out.println("Оповестили наблюдателя");
                observer.handle();
            }
        };

        Observer observer = new Observer() {
            @Override
            public void handle() {
                System.out.println("Я оповещен!");
            }
        };

        observable.addObserver(observer);
        observable.notifyObservers();
    }
}
