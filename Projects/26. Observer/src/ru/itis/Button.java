package ru.itis;

public class Button implements Observable {

    private Observer observers[] = new Observer[10];
    private int observersCount = 0;

    @Override
    public void addObserver(Observer observer) {
        this.observers[observersCount] = observer;
        this.observersCount++;
    }

    public void click() {
        notifyObservers();
    }

    @Override
    public void notifyObservers() {
        for (int i = 0; i < observersCount; i++) {
            observers[i].handle();
        }
    }
}
