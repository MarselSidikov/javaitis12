package ru.itis;

public class Main {

    public static void main(String[] args) {
        BetterUser.Builder builder = new BetterUser.Builder();

//        BetterUser.Builder b1 = builder.age(26);
//        BetterUser.Builder b2 = builder.firstName("Марсель");
//        BetterUser.Builder b3 = builder.height(1.85);
//
//        System.out.println(b1 == b2 && b2 == b3);

//        builder.age(26)
//                .firstName("Марсель")
//                .lastName("Сидиков")
//                .height(1.85);

        BetterUser user = BetterUser.builder()
                .age(26)
                .height(1.85)
                .firstName("Марсель")
                .build();

        int i = 0;

    }
}
