## Java Collection API

* Java Collection API - набор классов и интерфейсов, реализующих различные типы коллекций в Java.

* `interface Collection<E>` - базовый интерфейс, который описывает поведение коллекции.

```JAVA
int size();
boolean isEmpty();
boolean contains(Object o);
Object[] toArray();
boolean add(E e);
boolean remove(Object o);
void clear();
```

* `interface List<E> extends Collection<E>` - список, предполагает наличие порядка.

```JAVA
E get(int index);
E set(int index, E element);
void add(int index, E element);
int lastIndexOf(Object o);
int indexOf(Object o);
```

* `class ArrayList<E> implements List<E>` - список на основе массива.

```JAVA
// размер массива по умолчанию
private static final int DEFAULT_CAPACITY = 10;

// массив с данными
Object[] elementData;

// размер списка
private int size;

public boolean add(E e) {
		// удостовериться, что массив
		// вмещает новый элемент
        ensureCapacityInternal(size + 1);
        elementData[size++] = e;
        return true;
    }

// метод удостоверяется, что мы можем обеспечить minCapacity
private void ensureCapacityInternal(int minCapacity) {
		// удостоверяемся в явной емкости массива
        ensureExplicitCapacity(calculateCapacity(elementData, minCapacity));
    }

private void ensureExplicitCapacity(int minCapacity) {
        // если требуемый размер больше того, что есть
        if (minCapacity - elementData.length > 0)
        	// увеличиваем размер массива
            grow(minCapacity);
    }

private static int calculateCapacity(Object[] elementData, int minCapacity) {
		// если массив пустой
        if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
        	// возвращаем максимум из 10 и требуемого размера
            return Math.max(DEFAULT_CAPACITY, minCapacity);
        }
        // если массив не пустой, возвращает это же число
        return minCapacity;
    }

private void grow(int minCapacity) {
        // запоминаем старый размер
        int oldCapacity = elementData.length;
        // новый размер - это старый, увеличенный в полтора раза
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        // если новый размер оказался меньше требуемого
        if (newCapacity - minCapacity < 0)
        	// новый размер - и есть требуемый
            newCapacity = minCapacity;
        // если новый размер больше, максимально возможного
        if (newCapacity - MAX_ARRAY_SIZE > 0)
        	// новый размер - максимальное значение int
            newCapacity = hugeCapacity(minCapacity);
        // копирование старых данных в новый массив
        // вызывается System.arraycopy - нативная функция 
        elementData = Arrays.copyOf(elementData, newCapacity);
    }

 private static int hugeCapacity(int minCapacity) {
 		// если необходим размер, который меньше нуля
        if (minCapacity < 0)
            throw new OutOfMemoryError();
        // в противном случае, если требуемый размер больше допустимого
        // то допустимый - МАКСИМАЛЬНОЕ ЗНАЧЕНИЕ INT, в противном случае - допустимое
        return (minCapacity > MAX_ARRAY_SIZE) ?
            Integer.MAX_VALUE :
            MAX_ARRAY_SIZE;
    }
```

* `LinkedList<E> implements List<E>` - двунаправленный список на основе узлов.

```JAVA
// первый узел
Node<E> first;
// последний узел
Node<E> last;

private static class Node<E> {
        E item; // значение
        Node<E> next; // ссылка на следующий
        Node<E> prev; // ссылка на предыдущий
        ...
}

public boolean add(E e) {
        linkLast(e);
        return true;
}

void linkLast(E e) {
		// берем последний узел
        final Node<E> l = last;
        // создаем новый узел
        final Node<E> newNode = new Node<>(l, e, null);
        // последний узел равен новому
        last = newNode;
        // если список пустой
        if (l == null)
        	// первый узел - и есть новый
            first = newNode;
        else
        	// если не пустой - то следующий после последнего - новый
            l.next = newNode;
        size++;
}

public void addFirst(E e) {
        linkFirst(e);
}

  private void linkFirst(E e) {
        final Node<E> f = first;
        final Node<E> newNode = new Node<>(null, e, f);
        first = newNode;
        if (f == null)
            last = newNode;
        else
            f.prev = newNode;
        size++;
}
```